package com.ne0fhyklabs.media.musydra;

import android.graphics.Bitmap;
import com.ne0fhyklabs.media.musydra.utils.TrackInfoParcelable;

interface IApolloService
{
    void openFile(String path);
    void open(in long [] list, int position);
    int getQueuePosition();
    boolean isPlaying();
    void stop();
    void pause();
    void play();
    void prev();
    void next();
    long duration();
    long position();
    long seek(long pos);
    String getTrackName();
    String getAlbumName();
    long getAlbumId();
    String getArtistName();
    long getArtistId();
    void enqueue(in long [] list, int action);
    long [] getQueue();
    void setQueuePosition(int index);
    String getPath();
    long getAudioId();
    void setShuffleMode(int shufflemode);
    int getShuffleMode();
    int removeTracks(int first, int last);
    int removeTrack(long id);
    void setRepeatMode(int repeatmode);
    int getRepeatMode();
    int getMediaMountedCount();
    int getAudioSessionId();
	void addToFavorites(long id);
	void removeFromFavorites(long id);
	boolean isFavorite(long id);
    void toggleFavorite();
    
    void addToAutoConnectList(String groupName);
    void removeFromAutoConnectList(String groupName);
    String[] getAutoConnectList();
    String pingPeerGroup();
    String[] getFoundPeerGroupList();
    String getGroupName();
    void setGroupName(String groupName);
    String[] getJoinedPeerGroupList();
    void joinPeerGroup(String peerGroup);
    void leavePeerGroup(String peerGroupName);    
    boolean isSharingActive();
    void setSharingActive(boolean isActive);
    String getMusydraStateName(String stateKeyName);
    
    TrackInfoParcelable getCurrentTrack();
    TrackInfoParcelable[] getRemotePlaylist();
    TrackInfoParcelable[] getAllRemoteTracks();
    void openRemoteFile(in TrackInfoParcelable trackInfo);
}

