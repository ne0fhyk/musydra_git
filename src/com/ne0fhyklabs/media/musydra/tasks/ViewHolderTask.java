/**
 * 
 */

package com.ne0fhyklabs.media.musydra.tasks;

import static com.ne0fhyklabs.media.musydra.Constants.ALBUM_IMAGE;
import static com.ne0fhyklabs.media.musydra.Constants.ARTIST_IMAGE;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.views.ViewHolderGrid;
import com.ne0fhyklabs.media.musydra.views.ViewHolderList;

/**
 * @author Andrew Neal
 */
public class ViewHolderTask extends AsyncTask<String, Integer, Bitmap> {

    private final ViewHolderList mViewHolderList;

    private final ViewHolderGrid mViewHolderGrid;

    private final WeakReference<ImageView> imageViewReference;

    private final Context mContext;

    private final int mPosition;

    private final int choice;

    private final int holderChoice;

    private final AQuery aquery;

    private final ImageView mImageView;

    private final int albumart;

    private final WeakReference<Context> contextReference;

    private String url;

    private WeakReference<Bitmap> bitmapReference;

    public ViewHolderTask(ViewHolderList vh, ViewHolderGrid vhg, int position, Context c,
            int opt, int holderOpt, ImageView iv) {
        mViewHolderList = vh;
        mViewHolderGrid = vhg;
        mPosition = position;
        contextReference = new WeakReference<Context>(c);
        mContext = contextReference.get();
        choice = opt;
        holderChoice = holderOpt;
        imageViewReference = new WeakReference<ImageView>(iv);
        mImageView = imageViewReference.get();
        aquery = new AQuery(mContext);

        albumart = mContext.getResources().getInteger(R.integer.listview_album_art);
    }

    @Override
    protected Bitmap doInBackground(String... args) {
        if (choice == 0)
            url = MusydraUtils.getImageURL(args[0], ARTIST_IMAGE, mContext);
        if (choice == 1)
            url = MusydraUtils.getImageURL(args[0], ALBUM_IMAGE, mContext);
        bitmapReference = new WeakReference<Bitmap>(aquery.getCachedImage(url));
        if (holderChoice == 0) {
            return MusydraUtils.getResizedBitmap(bitmapReference.get(), albumart,
                    albumart);
        } else if (holderChoice == 1) {
            return bitmapReference.get();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result != null && imageViewReference != null && holderChoice == 0
                && mViewHolderList != null && mViewHolderList.position == mPosition)
            mImageView.setImageBitmap(result);
        if (result != null && imageViewReference != null && holderChoice == 1
                && mViewHolderGrid != null && mViewHolderGrid.position == mPosition)
            mImageView.setImageBitmap(result);
        super.onPostExecute(result);
    }
}
