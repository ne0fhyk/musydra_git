/**
 *
 */

package com.ne0fhyklabs.media.musydra.activities;

import static com.ne0fhyklabs.media.musydra.Constants.MIME_TYPE;
import static com.ne0fhyklabs.media.musydra.Constants.PLAYLIST_QUEUE;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.MediaStore.Audio;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.ne0fhyklabs.media.musydra.fragments.grid.QuickQueueFragment;

/**
 * @author Andrew Neal
 */
public class QuickQueue extends FragmentActivity {

    @Override
    protected void onCreate(Bundle icicle) {
        // This needs to be called first
        super.onCreate(icicle);

        // Control Media volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        Bundle bundle = new Bundle();
        bundle.putString(MIME_TYPE, Audio.Playlists.CONTENT_TYPE);
        bundle.putLong(BaseColumns._ID, PLAYLIST_QUEUE);

        Fragment quickQueueFragment = new QuickQueueFragment();
        quickQueueFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, quickQueueFragment)
                .commit();
    }
}
