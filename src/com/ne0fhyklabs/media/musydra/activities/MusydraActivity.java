/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhyklabs.media.musydra.activities;

import java.util.Arrays;
import java.util.HashMap;

import android.app.ActionBar;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.audiofx.AudioEffect;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.ne0fhyklabs.media.musydra.BottomActionBarControlsFragment;
import com.ne0fhyklabs.media.musydra.BottomActionBarFragment;
import com.ne0fhyklabs.media.musydra.IApolloService;
import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.adapters.PagerAdapter;
import com.ne0fhyklabs.media.musydra.fragments.MusydraFragment;
import com.ne0fhyklabs.media.musydra.fragments.list.RemoteTracksFragment;
import com.ne0fhyklabs.media.musydra.fragments.section.MusicLibrary;
import com.ne0fhyklabs.media.musydra.fragments.section.PlaylistsFragment;
import com.ne0fhyklabs.media.musydra.fragments.section.StreamBrowserFragment;
import com.ne0fhyklabs.media.musydra.preferences.SettingsHolder;
import com.ne0fhyklabs.media.musydra.service.ServiceToken;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;

public class MusydraActivity extends FragmentActivity {

    private static final String TAG = MusydraActivity.class.getSimpleName();

    private static final String[] NAVIGATION_SECTION_TAG = { MusicLibrary.TAG, StreamBrowserFragment.TAG,
                                                            PlaylistsFragment.TAG };

    private static HashMap<String, Integer> sTitleIdForTag = new HashMap<String, Integer>();
    static {
        sTitleIdForTag.put(MusicLibrary.TAG, MusicLibrary.TITLE_ID);
        sTitleIdForTag.put(StreamBrowserFragment.TAG, StreamBrowserFragment.TITLE_ID);
        sTitleIdForTag.put(PlaylistsFragment.TAG, PlaylistsFragment.TITLE_ID);
    }

    public static final String KEY_FRAGMENT_TAG = "section_tag";
    public static final String DEFAULT_FRAGMENT_TAG = NAVIGATION_SECTION_TAG[0];

    protected DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;

    protected ActionBarDrawerToggle mDrawerToggle;

    protected ViewPager mBottomBarPager;

    protected CharSequence mTitle;
    protected CharSequence mDrawerTitle;
    protected String[] mMusydraSections;

    protected ServiceToken mToken;
    protected ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            MusicUtils.mService = null;

            sendBroadcast(new Intent(MusydraUtils.ACTION_SERVICE_STATE)
            .putExtra(MusydraUtils.KEY_IS_SERVICE_CONNECTED, false));
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicUtils.mService = IApolloService.Stub.asInterface(service);

            sendBroadcast(new Intent(MusydraUtils.ACTION_SERVICE_STATE)
            .putExtra(MusydraUtils.KEY_IS_SERVICE_CONNECTED, true));
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.musydra_activity);

        // Control Media volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        final Resources res = getResources();

        mMusydraSections = new String[NAVIGATION_SECTION_TAG.length];
        for ( int i = 0; i < NAVIGATION_SECTION_TAG.length; i++ ) {
            String sectionTag = NAVIGATION_SECTION_TAG[i];
            Integer sectionTitleId = sTitleIdForTag.get(sectionTag);
            mMusydraSections[i] = res.getString(sectionTitleId);
        }

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.ma_drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.ma_left_drawer);

        // Set a custom drawer shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // Set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mMusydraSections));
        mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectNavigationSection(position);
            }

        });

        // enable actionbar app icon to behave as action to toggle nav drawer
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the proper interactions
        // between the sliding drawer, and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                                                  R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View view) {
                actionBar.setTitle(mTitle);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                actionBar.setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // Update the BottomActionBar
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new BottomActionBarFragment());
        pagerAdapter.addFragment(new BottomActionBarControlsFragment());

        mBottomBarPager = (ViewPager) findViewById(R.id.bottomActionBarPager);
        mBottomBarPager.setAdapter(pagerAdapter);

        if ( savedInstanceState == null ) {
            selectNavigationSection(0);
            handleIntentExtras(getIntent());
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntentExtras(intent);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Bind to Service
        mToken = MusicUtils.bindToService(this, mServiceConnection);
    }

    @Override
    public void onStop() {
        super.onStop();

        if ( MusicUtils.mService != null ) {
            MusicUtils.unbindFromService(mToken);
            mToken = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.library_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*
     * Called whenever we call invalidateOptionsMenu()
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // TODO: if the navigation drawer is open, hide action items related to the content view
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if ( mDrawerToggle.onOptionsItemSelected(item) )
            return true;

        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, SettingsHolder.class));
                return true;

            case R.id.equalizer:
                Intent i = new Intent(AudioEffect.ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL);
                i.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, MusicUtils.getCurrentAudioId());
                startActivityForResult(i, MusydraUtils.EFFECTS_PANEL);
                return true;

            case R.id.shuffle_all:
                // TODO Only shuffle the tracks that are shown
                MusydraUtils.shuffleAll(getApplicationContext());
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggle
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        MusydraFragment contentFragment = getContentFragment();
        if ( contentFragment != null ) {
            String fragmentTag = contentFragment.toString();
            if ( Arrays.binarySearch(NAVIGATION_SECTION_TAG, fragmentTag) >= 0 ) {
                // This is a section fragment
                finish();
                return;
            }
        }

        super.onBackPressed();
    }

    public void setDrawerIndicatorEnabled(boolean enable) {
        if ( mDrawerToggle != null )
            mDrawerToggle.setDrawerIndicatorEnabled(enable);
    }

    protected void selectNavigationSection(int position) {
        // Update the main content by replacing fragments
        final String sectionTag = NAVIGATION_SECTION_TAG[position];
        addFragment(sectionTag, new Bundle());

        // Update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mMusydraSections[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    public void addFragment(String fragmentTag, Bundle args) {
        final FragmentManager fm = getSupportFragmentManager();

        // Get the current fragment, and check if it matches
        MusydraFragment contentFragment = (MusydraFragment) fm.findFragmentById(R.id.ma_content_frame);
        if ( contentFragment != null && contentFragment.toString().equals(fragmentTag) ) {
            // The current fragment matches the fragment we are trying to insert. We're done.
            return;
        }

        if ( args == null )
            args = new Bundle();

        contentFragment = (MusydraFragment) fm.findFragmentByTag(fragmentTag);
        if ( contentFragment == null ) {
            // The fragment we're trying to insert is not stored in the activity. Create a new one.

            if ( StreamBrowserFragment.TAG.equals(fragmentTag) )
                contentFragment = new StreamBrowserFragment();
            else if ( PlaylistsFragment.TAG.equals(fragmentTag) )
                contentFragment = new PlaylistsFragment();
            else if ( RemoteTracksFragment.TAG.equals(fragmentTag) )
                contentFragment = new RemoteTracksFragment();
            else {
                contentFragment = new MusicLibrary();
            }

            contentFragment.setArguments(args);
        }

        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.ma_content_frame, contentFragment, fragmentTag);
        ft.addToBackStack(fragmentTag);
        ft.commit();
    }

    public void popContentFragment() {
        getSupportFragmentManager().popBackStack();
    }

    protected MusydraFragment getContentFragment() {
        return (MusydraFragment) getSupportFragmentManager().findFragmentById(R.id.ma_content_frame);
    }

    protected void handleIntentExtras(Intent intent) {
        Bundle extras = intent.getExtras();

        if ( extras != null && extras.containsKey(KEY_FRAGMENT_TAG) ) {
            String fragmentTag = extras.getString(KEY_FRAGMENT_TAG, DEFAULT_FRAGMENT_TAG);
            addFragment(fragmentTag, new Bundle());
        }
    }

    private void clearBackStack() {
        FragmentManager fm = getSupportFragmentManager();
        int backStackCount = fm.getBackStackEntryCount();
        for ( int i = 0; i < backStackCount; i++ ) {
            fm.popBackStack();
        }
    }
}
