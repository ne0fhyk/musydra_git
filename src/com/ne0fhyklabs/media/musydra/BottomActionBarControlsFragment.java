/**
 *
 */

package com.ne0fhyklabs.media.musydra;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.ne0fhyklabs.media.musydra.service.MusydraService;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;

/**
 * @author Andrew Neal
 */
public class BottomActionBarControlsFragment extends Fragment {

    private ImageButton mRepeat, mPrev, mPlay, mNext, mShuffle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.bottom_action_bar_controls, container,
                false);

        mRepeat = (ImageButton) root.findViewById(R.id.bottom_action_bar_repeat);
        mRepeat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                cycleRepeat();
            }
        });

        mPrev = (ImageButton) root.findViewById(R.id.bottom_action_bar_previous);
        mPrev.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MusicUtils.mService == null)
                    return;
                try {
                    if (MusicUtils.mService.position() < 2000) {
                        MusicUtils.mService.prev();
                    } else {
                        MusicUtils.mService.seek(0);
                        MusicUtils.mService.play();
                    }
                } catch (RemoteException ex) {
                    ex.printStackTrace();
                }
            }
        });

        mPlay = (ImageButton) root.findViewById(R.id.bottom_action_bar_play);
        mPlay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                doPauseResume();
            }
        });

        mNext = (ImageButton) root.findViewById(R.id.bottom_action_bar_next);
        mNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MusicUtils.mService == null)
                    return;
                try {
                    MusicUtils.mService.next();
                } catch (RemoteException ex) {
                    ex.printStackTrace();
                }
            }
        });

        mShuffle = (ImageButton) root.findViewById(R.id.bottom_action_bar_shuffle);
        mShuffle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleShuffle();
            }
        });

        return root;
    }

    /**
     * Update everything as the meta or playstate changes
     */
    private final BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setPauseButtonImage();
            setShuffleButtonImage();
            setRepeatButtonImage();
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter f = new IntentFilter();
        f.addAction(MusydraService.PLAYSTATE_CHANGED);
        getActivity().registerReceiver(mStatusListener, new IntentFilter(f));
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(mStatusListener);
    }

    /**
     * Cycle repeat states
     */
    private void cycleRepeat() {
        if (MusicUtils.mService == null) {
            return;
        }
        try {
            int mode = MusicUtils.mService.getRepeatMode();
            if (mode == MusydraService.REPEAT_NONE) {
                MusicUtils.mService.setRepeatMode(MusydraService.REPEAT_ALL);
                MusydraUtils.showToast(getActivity(), R.string.repeat_all);
            } else if (mode == MusydraService.REPEAT_ALL) {
                MusicUtils.mService.setRepeatMode(MusydraService.REPEAT_CURRENT);
                if (MusicUtils.mService.getShuffleMode() != MusydraService.SHUFFLE_NONE) {
                    MusicUtils.mService.setShuffleMode(MusydraService.SHUFFLE_NONE);
                    setShuffleButtonImage();
                }
                MusydraUtils.showToast(getActivity(), R.string.repeat_one);
            } else {
                MusicUtils.mService.setRepeatMode(MusydraService.REPEAT_NONE);
                MusydraUtils.showToast(getActivity(), R.string.repeat_off);
            }
            setRepeatButtonImage();
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Play and pause music
     */
    private void doPauseResume() {
        try {
            if (MusicUtils.mService != null) {
                if (MusicUtils.mService.isPlaying()) {
                    MusicUtils.mService.pause();
                } else {
                    MusicUtils.mService.play();
                }
            }
            setPauseButtonImage();
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Set the shuffle mode
     */
    private void toggleShuffle() {
        if (MusicUtils.mService == null) {
            return;
        }
        try {
            int shuffle = MusicUtils.mService.getShuffleMode();
            if (shuffle == MusydraService.SHUFFLE_NONE) {
                MusicUtils.mService.setShuffleMode(MusydraService.SHUFFLE_NORMAL);
                if (MusicUtils.mService.getRepeatMode() == MusydraService.REPEAT_CURRENT) {
                    MusicUtils.mService.setRepeatMode(MusydraService.REPEAT_ALL);
                    setRepeatButtonImage();
                }
                MusydraUtils.showToast(getActivity(), R.string.shuffle_on);
            } else if (shuffle == MusydraService.SHUFFLE_NORMAL
                    || shuffle == MusydraService.SHUFFLE_AUTO) {
                MusicUtils.mService.setShuffleMode(MusydraService.SHUFFLE_NONE);
                MusydraUtils.showToast(getActivity(), R.string.shuffle_off);
            }
            setShuffleButtonImage();
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Set the repeat images
     */
    private void setRepeatButtonImage() {
        if (MusicUtils.mService == null)
            return;
        try {
            switch (MusicUtils.mService.getRepeatMode()) {
                case MusydraService.REPEAT_ALL:
                    mRepeat.setImageResource(R.drawable.apollo_holo_light_repeat_all);
                    break;
                case MusydraService.REPEAT_CURRENT:
                    mRepeat.setImageResource(R.drawable.apollo_holo_light_repeat_one);
                    break;
                default:
                    mRepeat.setImageResource(R.drawable.apollo_holo_light_repeat_normal);
                    break;
            }
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Set the shuffle images
     */
    private void setShuffleButtonImage() {
        if (MusicUtils.mService == null)
            return;
        try {
            switch (MusicUtils.mService.getShuffleMode()) {
                case MusydraService.SHUFFLE_NONE:
                    mShuffle.setImageResource(R.drawable.apollo_holo_light_shuffle_normal);
                    break;
                case MusydraService.SHUFFLE_AUTO:
                    mShuffle.setImageResource(R.drawable.apollo_holo_light_shuffle_on);
                    break;
                default:
                    mShuffle.setImageResource(R.drawable.apollo_holo_light_shuffle_on);
                    break;
            }
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Set the play and pause image
     */
    private void setPauseButtonImage() {
        try {
            if (MusicUtils.mService != null && MusicUtils.mService.isPlaying()) {
                mPlay.setImageResource(R.drawable.apollo_holo_light_pause);
            } else {
                mPlay.setImageResource(R.drawable.apollo_holo_light_play);
            }
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

}
