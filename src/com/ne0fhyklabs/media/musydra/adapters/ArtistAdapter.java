
package com.ne0fhyklabs.media.musydra.adapters;

import static com.ne0fhyklabs.media.musydra.Constants.ARTIST_IMAGE;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.fragments.grid.ArtistsFragment;
import com.ne0fhyklabs.media.musydra.tasks.LastfmGetArtistImages;
import com.ne0fhyklabs.media.musydra.tasks.ViewHolderTask;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.views.ViewHolderGrid;

/**
 * @author Andrew Neal
 */
public class ArtistAdapter extends SimpleCursorAdapter {

    private AnimationDrawable mPeakOneAnimation, mPeakTwoAnimation;

    private WeakReference<ViewHolderGrid> holderReference;

    public ArtistAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);

        if (view != null) {

            holderReference = new WeakReference<ViewHolderGrid>(new ViewHolderGrid(view));
            view.setTag(holderReference.get());

        }

        // AQuery
        final AQuery aq = new AQuery(view);

        // Artist Name
        String artistName = mCursor.getString(ArtistsFragment.mArtistNameIndex);
        holderReference.get().mViewHolderLineOne.setText(artistName);

        // Number of albums
        int albums_plural = mCursor.getInt(ArtistsFragment.mArtistNumAlbumsIndex);
        boolean unknown = artistName == null || artistName.equals(MediaStore.UNKNOWN_STRING);
        String numAlbums = MusicUtils.makeAlbumsLabel(mContext, albums_plural, 0, unknown);
        holderReference.get().mViewHolderLineTwo.setText(numAlbums);

        holderReference.get().position = position;
        if (aq.shouldDelay(position, view, parent, "")) {
            holderReference.get().mViewHolderImage.setImageDrawable(null);
        } else {
            // Check for missing artist images and cache them
            if (MusydraUtils.getImageURL(artistName, ARTIST_IMAGE, mContext) == null) {
                new LastfmGetArtistImages(mContext).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, artistName);
            } else {
                new ViewHolderTask(null, holderReference.get(), position, mContext, 0, 1,
                        holderReference.get().mViewHolderImage).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, artistName);
            }
        }
        // Now playing indicator
        long currentartistid = MusicUtils.getCurrentArtistId();
        long artistid = mCursor.getLong(ArtistsFragment.mArtistIdIndex);
        if (currentartistid == artistid) {
            holderReference.get().mPeakOne.setImageResource(R.anim.peak_meter_1);
            holderReference.get().mPeakTwo.setImageResource(R.anim.peak_meter_2);
            mPeakOneAnimation = (AnimationDrawable)holderReference.get().mPeakOne.getDrawable();
            mPeakTwoAnimation = (AnimationDrawable)holderReference.get().mPeakTwo.getDrawable();
            try {
                if (MusicUtils.mService.isPlaying()) {
                    mPeakOneAnimation.start();
                    mPeakTwoAnimation.start();
                } else {
                    mPeakOneAnimation.stop();
                    mPeakTwoAnimation.stop();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            holderReference.get().mPeakOne.setImageResource(0);
            holderReference.get().mPeakTwo.setImageResource(0);
        }
        return view;
    }
}
