
package com.ne0fhyklabs.media.musydra.adapters;

import static com.ne0fhyklabs.media.musydra.Constants.ALBUM_IMAGE;
import static com.ne0fhyklabs.media.musydra.Constants.ARTIST_IMAGE;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.ne0fhyklabs.media.musydra.fragments.grid.QuickQueueFragment;
import com.ne0fhyklabs.media.musydra.tasks.LastfmGetAlbumImages;
import com.ne0fhyklabs.media.musydra.tasks.LastfmGetArtistImages;
import com.ne0fhyklabs.media.musydra.tasks.ViewHolderQueueTask;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.views.ViewHolderQueue;

/**
 * @author Andrew Neal
 */
public class QuickQueueAdapter extends SimpleCursorAdapter {

    private WeakReference<ViewHolderQueue> holderReference;

    public QuickQueueAdapter(Context context, int layout, Cursor c, String[] from, int[] to,
            int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);

        if (view != null) {

            holderReference = new WeakReference<ViewHolderQueue>(new ViewHolderQueue(view));
            view.setTag(holderReference.get());

        }

        // AQuery
        final AQuery aq = new AQuery(view);

        // Artist Name
        String artistName = mCursor.getString(QuickQueueFragment.mArtistIndex);

        // Album name
        String albumName = mCursor.getString(QuickQueueFragment.mAlbumIndex);

        // Track name
        String trackName = mCursor.getString(QuickQueueFragment.mTitleIndex);
        holderReference.get().mTrackName.setText(trackName);

        holderReference.get().position = position;
        // Artist Image
        if (aq.shouldDelay(position, view, parent, "")) {
            holderReference.get().mArtistImage.setImageDrawable(null);
        } else {
            // Check for missing artist images and cache them
            if (MusydraUtils.getImageURL(artistName, ARTIST_IMAGE, mContext) == null) {
                new LastfmGetArtistImages(mContext).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, artistName);
            } else {
                new ViewHolderQueueTask(holderReference.get(), position, mContext, 0, 0,
                        holderReference.get().mArtistImage).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, artistName);
            }
        }

        // Album Image
        if (aq.shouldDelay(position, view, parent, "")) {
            holderReference.get().mAlbumArt.setImageDrawable(null);
        } else {
            // Check for missing album images and cache them
            if (MusydraUtils.getImageURL(albumName, ALBUM_IMAGE, mContext) == null) {
                new LastfmGetAlbumImages(mContext, null, 0).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, artistName, albumName);
            } else {
                new ViewHolderQueueTask(holderReference.get(), position, mContext, 1, 1,
                        holderReference.get().mAlbumArt).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, albumName);
            }
        }
        return view;
    }
}
