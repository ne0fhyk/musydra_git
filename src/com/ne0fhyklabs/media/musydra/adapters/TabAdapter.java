
package com.ne0fhyklabs.media.musydra.adapters;

import android.view.View;

public interface TabAdapter {
    public View getView(int position);
}
