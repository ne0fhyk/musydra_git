/**
 * 
 */

package com.ne0fhyklabs.media.musydra.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.activities.AudioPlayerHolder;
import com.ne0fhyklabs.media.musydra.activities.QuickQueue;
import com.ne0fhyklabs.media.musydra.tasks.GetCachedImages;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.utils.TrackInfo;
import com.ne0fhyklabs.media.musydra.utils.TrackInfoParcelable;

/**
 * @author Andrew Neal
 */
public class BottomActionBar extends LinearLayout implements OnClickListener,
OnLongClickListener {

    private static final String TAG = MusydraUtils.APP_TAG + ".BottomActionBar";

    public BottomActionBar(Context context) {
        super(context);
    }

    public BottomActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(this);
        setOnLongClickListener(this);
    }

    public BottomActionBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Updates the bottom ActionBar's info
     * 
     * @param activity
     * @throws RemoteException
     */
    public void updateBottomActionBar(Activity activity) {
        View bottomActionBar = activity.findViewById(R.id.bottom_action_bar);
        if ( bottomActionBar == null ) {
            return;
        }

        if ( MusicUtils.mService != null ) {
            TrackInfo currentTrack = null;
            try {
                TrackInfoParcelable trackInfoParcelable = MusicUtils.mService
                        .getCurrentTrack();
                if ( trackInfoParcelable != null )
                    currentTrack = trackInfoParcelable.getTrackInfo();
            } catch (RemoteException e) {
                Log.e(TAG, "Remote exception while retrieving current track.", e);
                return;
            }

            if ( currentTrack == null )
                return;

            // Track name
            TextView mTrackName = (TextView) bottomActionBar
                    .findViewById(R.id.bottom_action_bar_track_name);
            mTrackName.setText(currentTrack.mTitle);

            // Artist name
            TextView mArtistName = (TextView) bottomActionBar
                    .findViewById(R.id.bottom_action_bar_artist_name);
            mArtistName.setText(currentTrack.mArtist);

            // Album art
            ImageView mAlbumArt = (ImageView) bottomActionBar
                    .findViewById(R.id.bottom_action_bar_album_art);

            new GetCachedImages(activity, 1, mAlbumArt).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, currentTrack.mAlbum);

            // Favorite image
            ImageButton mFavorite = (ImageButton) bottomActionBar
                    .findViewById(R.id.bottom_action_bar_item_one);

            MusicUtils.setFavoriteImage(mFavorite);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bottom_action_bar:
                Intent intent = new Intent();
                intent.setClass(v.getContext(), AudioPlayerHolder.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(intent);
                break;
            default:
                break;
        }

    }

    @Override
    public boolean onLongClick(View v) {
        Context context = v.getContext();
        context.startActivity(new Intent(context, QuickQueue.class));
        return true;
    }
}
