/**
 * 
 */

package com.ne0fhyklabs.media.musydra.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.tasks.FetchAlbumImages;
import com.ne0fhyklabs.media.musydra.tasks.FetchArtistImages;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;

/**
 * @author Andrew Neal
 */
public class BottomActionBarItem extends ImageButton implements OnLongClickListener,
        OnClickListener {

    private final Context mContext;

    public BottomActionBarItem(Context context) {
        super(context);
        mContext = context;
    }

    public BottomActionBarItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnLongClickListener(this);
        setOnClickListener(this);
        mContext = context;
    }

    public BottomActionBarItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    @Override
    public boolean onLongClick(View v) {
        Toast.makeText(getContext(), v.getContentDescription(), Toast.LENGTH_SHORT)
                .show();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bottom_action_bar_item_one:
                MusicUtils.toggleFavorite();
                MusicUtils.setFavoriteImage(this);
                break;
            case R.id.bottom_action_bar_item_two:
                ((Activity) mContext).onSearchRequested();
                break;
            case R.id.bottom_action_bar_item_three:
                MusydraUtils.showPopupMenu(mContext, v);
                break;
            default:
                break;
        }
    }

    /**
     * Manually re-fetch artist imgages. Maybe the user wants to update them or
     * something went wrong the first time around.
     */
    public void initArtistImages() {
        FetchArtistImages getArtistImages = new FetchArtistImages(mContext, 1);
        getArtistImages.runTask();
    }

    /**
     * Manually fetch all of the album art.
     */
    public void initAlbumImages() {
        FetchAlbumImages getAlbumImages = new FetchAlbumImages(mContext, 1);
        getAlbumImages.runTask();
    }
}
