package com.ne0fhyklabs.media.musydra.service.webserver;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.TrackInfo;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;

public class WebServer extends NanoHTTPD {

    private static final String TAG = WebServer.class.getSimpleName();

    private static final boolean DEBUG_FLAG = true;

    public static final String HEADER_ACCEPT_RANGES = "Accept-Ranges";
    public static final String HEADER_CONTENT_LENGTH = "content-length";
    public static final String HEADER_CONTENT_RANGE = "Content-Range";
    public static final String HEADER_ETAG = "ETag";
    public static final String HEADER_RANGE = "range";

    public static final String KEY_TRACK_ID = "track_id";
    public static final String KEY_DOWNLOAD_FLAG = "download";

    public static final String MIME_AUDIO = "audio/mpeg";
    public static final String MIME_DOWNLOAD = "application/force-download";
    public static final String MIME_IMG = "image/png";

    private static final char RANGE_MINUS = '-';

    public static final String URI_SERVER_TIME = "/server_time";
    public static final String URI_SERVER_ICON = "/favicon.ico";

    public static final String VALUE_ETAG_IF_NONE_MATCH = "if-none-match";
    private static final String VALUE_RANGE_PREFIX = "bytes=";

    private final Context mContext;
    private AtomicBoolean mIsRunning;

    public WebServer(Context context) {
        super(0);
        mContext = context;
        mIsRunning = new AtomicBoolean(false);
    }

    @Override
    public void start() {
        if ( mIsRunning.compareAndSet(false, true) ) {
            try {
                super.start();
                Log.i(TAG, "Starting media server @ " + getServerIpAddress() + ":" + getServerPort());
            } catch (IOException e) {
                mIsRunning.set(false);
            }
        }
    }

    @Override
    public void stop() {
        if ( mIsRunning.compareAndSet(true, false) ) {
            super.stop();
            Log.i(TAG, "Stopping web server @ " + getServerIpAddress() + ":" + getServerPort());
        }
    }

    @Override
    public Response serve(String uri, Method method, Map<String, String> header, Map<String, String> parms,
                          Map<String, String> files) {

        if ( DEBUG_FLAG ) {
            Log.d(TAG, method + " '" + uri + "' ");

            Iterator<Map.Entry<String, String>> e = header.entrySet().iterator();
            while (e.hasNext()) {
                Map.Entry<String, String> entry = e.next();
                Log.d(TAG, "  HDR: '" + entry.getKey() + "' = '" + entry.getValue() + "'");
            }
            e = parms.entrySet().iterator();
            while (e.hasNext()) {
                Map.Entry<String, String> entry = e.next();
                Log.d(TAG, "  PRM: '" + entry.getKey() + "' = '" + entry.getValue() + "'");
            }
            e = files.entrySet().iterator();
            while (e.hasNext()) {
                Map.Entry<String, String> entry = e.next();
                Log.d(TAG, "  UPLOADED: '" + entry.getKey() + "' = '" + entry.getValue() + "'");
            }
        }

        Response response = null;
        if ( !parms.isEmpty() && uri.equals("/") ) {

            String trackIdValue = parms.get(KEY_TRACK_ID);
            if ( trackIdValue != null ) {
                try {
                    int trackId = Integer.valueOf(trackIdValue);
                    TrackInfo trackInfo = getTrackInfo(trackId);
                    if ( trackInfo != null ) {

                        String downloadFlagValue = parms.get(KEY_DOWNLOAD_FLAG);
                        boolean downloadFlag = downloadFlagValue != null ? Boolean.valueOf(downloadFlagValue)
                                                                         : false;
                        String mimeType = downloadFlag ? MIME_DOWNLOAD : trackInfo.mMimeType;

                        //Calculate etag
                        String etag = Integer
                                .toHexString((trackInfo.mTrackUri + trackInfo.mDateModified + trackInfo.mSize)
                                             .hashCode());

                        // Support simple skipping
                        long startFrom = 0;
                        long endAt = -1;
                        String range = header.get(HEADER_RANGE);
                        if ( range != null && range.startsWith(VALUE_RANGE_PREFIX) ) {
                            range = range.substring(VALUE_RANGE_PREFIX.length());
                            int minus = range.indexOf(RANGE_MINUS);
                            try {
                                if ( minus > 0 ) {
                                    startFrom = Long.parseLong(range.substring(0, minus));
                                    if ( minus < range.length() - 1 )
                                        endAt = Long.parseLong(range.substring(minus + 1));
                                }
                            } catch (NumberFormatException e) {
                                Log.e(TAG, e.getMessage(), e);
                            }
                        }

                        // Change return code, and add content-range header when skipping is
                        // requested
                        long trackSize = trackInfo.mSize;
                        if ( range != null && startFrom >= 0 ) {
                            if ( startFrom >= trackSize ) {
                                response = new Response(Response.Status.RANGE_NOT_SATISFIABLE,
                                                        MIME_PLAINTEXT, "");
                                response.addHeader(HEADER_CONTENT_RANGE, "bytes 0-0/" + trackSize);
                                response.addHeader(HEADER_ETAG, etag);
                            }
                            else {
                                if ( endAt < 0 )
                                    endAt = trackSize - 1;
                                long newLen = endAt - startFrom + 1;
                                if ( newLen < 0 )
                                    newLen = 0;

                                final long dataLen = newLen;
                                FileInputStream trackData = new FileInputStream(trackInfo.mTrackUri) {
                                    @Override
                                    public int available() throws IOException {
                                        return (int) dataLen;
                                    }
                                };
                                trackData.skip(startFrom);

                                response = new Response(Response.Status.PARTIAL_CONTENT, mimeType, trackData);
                                response.addHeader(HEADER_CONTENT_LENGTH, String.valueOf(dataLen));
                                response.addHeader(HEADER_CONTENT_RANGE, "bytes " + startFrom + "-" + endAt +
                                                   "/" + trackSize);
                                response.addHeader(HEADER_ETAG, etag);
                            }
                        }
                        else {
                            if ( etag.equals(header.get(VALUE_ETAG_IF_NONE_MATCH)) )
                                response = new Response(Response.Status.NOT_MODIFIED, mimeType, "");
                            else {
                                response = new Response(Status.OK, mimeType,
                                                        new FileInputStream(trackInfo.mTrackUri));
                                response.addHeader(HEADER_CONTENT_LENGTH, String.valueOf(trackSize));
                                response.addHeader(HEADER_ETAG, etag);
                            }
                        }
                    }
                    else {
                        Log.w(TAG, "Invalid track id: " + trackId);
                    }
                } catch (NumberFormatException e) {
                    Log.e(TAG, "Invalid format for id value: " + trackIdValue, e);
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "Unable to locate track file.", e);
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }
        else if ( uri.equals(URI_SERVER_TIME) ) {
            response = new Response(Long.toString(System.currentTimeMillis()));
        }
        else if ( uri.equals(URI_SERVER_ICON) ) {
            response = new Response(Status.OK, MIME_IMG, mContext.getResources()
                                    .openRawResource(R.drawable.ic_launcher));
        }

        if ( response == null )
            response = new Response(getHomePage());

        // Announce that the file server accepts partial content request
        response.addHeader(HEADER_ACCEPT_RANGES, "bytes");
        return response;
    }

    public String getServerIpAddress() {
        try {
            for ( Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en
                    .hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for ( Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr
                        .hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if ( !inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress() )
                        return inetAddress.getHostAddress();
                }
            }
        } catch (SocketException e) {
            Log.e(TAG, "Socket exception", e);
        }
        return null;
    }

    public int getServerPort() {
        if ( myServerSocket == null )
            return -1;
        return myServerSocket.getLocalPort();
    }

    public String getTrackUri(long trackId, boolean download) {
        return "http://" + getServerIpAddress() + ":" + getServerPort() + "/?" + KEY_TRACK_ID + "=" +
                trackId + "&" + KEY_DOWNLOAD_FLAG + "=" + download;
    }

    public boolean isRunning() {
        return mIsRunning.get();
    }

    private String getHomePage() {
        String appName = mContext.getResources().getString(R.string.app_name);
        String homePage = "<html><head></head><body><h1>" + appName + "<h1>";

        try {
            String versionName = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
            homePage += "<p>Version " + versionName + "</p>";
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Unable to resolve version name", e);
        }

        homePage += "</body></html>";
        return homePage;
    }

    private TrackInfo getTrackInfo(int trackId) {
        return MusicUtils.getTrackInfo(mContext, trackId);
    }

    public static long getServerTimestamp(URL serverUrl) {
        try {
            URL serverTimestampUrl = new URL(serverUrl, URI_SERVER_TIME);

            HttpURLConnection conn = (HttpURLConnection) serverTimestampUrl.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoOutput(true);

            InputStream in = new BufferedInputStream(conn.getInputStream());
            final String output = readStream(in);
            in.close();

            return Long.parseLong(output.trim());

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return -1;
    }

    private static String readStream(InputStream in) throws IOException {
        String line;
        StringBuilder output = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        while ((line = reader.readLine()) != null) {
            output.append(line + "\n");
        }

        reader.close();
        return output.toString();
    }

}
