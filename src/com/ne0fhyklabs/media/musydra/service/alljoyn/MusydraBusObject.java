package com.ne0fhyklabs.media.musydra.service.alljoyn;

import java.lang.ref.WeakReference;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusObject;

import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraStateKey;
import com.ne0fhyklabs.media.musydra.utils.TrackInfo;

public class MusydraBusObject implements RemotePlayerInterface, BusObject {

    private final static String TAG = MusydraUtils.APP_TAG + ".RemotePlayer.MusydraBusObject";

    private final WeakReference<RemotePlayer> mRemotePlayerReference;

    public MusydraBusObject(RemotePlayer remotePlayer) {
        mRemotePlayerReference = new WeakReference<RemotePlayer>(remotePlayer);
    }

    protected RemotePlayer getRemotePlayer() {
        RemotePlayer remotePlayer = mRemotePlayerReference.get();
        if ( remotePlayer == null )
            throw new IllegalStateException("Weak reference to remote player was cleaned.");

        return remotePlayer;
    }

    @Override
    public void setDataSource(TrackInfo mediaInfo) throws BusException {}

    @Override
    public void start(long eventTime) throws BusException {}

    @Override
    public void pause(long eventTime) throws BusException {}

    @Override
    public void stop() throws BusException {}

    @Override
    public void setVolume(float vol) throws BusException {}

    @Override
    public void seek(long eventTime, long whereto) throws BusException {}

    @Override
    public void disconnect(String groupName) throws BusException {}

    @Override
    public String ping() throws BusException {
        return getRemotePlayer().getGroupName();
    }

    @Override
    public String getMultiPlayerState() throws BusException {
        return getRemotePlayer().getState(MusydraStateKey.MULTI_PLAYER_STATE).name();
    }

    @Override
    public long[] getCurrentTrackPosition() throws BusException {
        final RemotePlayer remotePlayer = getRemotePlayer();
        long[] positionAndTime = new long[] { System.currentTimeMillis(), remotePlayer.position() };
        return positionAndTime;
    }

    @Override
    public TrackInfo getCurrentTrack() throws BusException {
        final RemotePlayer remotePlayer = getRemotePlayer();
        TrackInfo currentTrack = remotePlayer.mService.getCurrentTrack();
        currentTrack.mTrackUri = remotePlayer.mWebServer.getTrackUri(currentTrack.mTrackId, false);
        currentTrack.mDownloadUri = remotePlayer.mWebServer.getTrackUri(currentTrack.mTrackId, true);
        currentTrack.mTrackId = -1;
        return currentTrack;
    }

    @Override
    public TrackInfo[] getPlaylist() throws BusException {
        final RemotePlayer remotePlayer = getRemotePlayer();

        TrackInfo[] playlist = remotePlayer.mService.getPlaylist();
        for ( int i = 0; i < playlist.length; i++ ) {
            playlist[i].mTrackUri = remotePlayer.mWebServer.getTrackUri(playlist[i].mTrackId, false);
            playlist[i].mDownloadUri = remotePlayer.mWebServer.getTrackUri(playlist[i].mTrackId, true);
            playlist[i].mTrackId = -1;
        }
        return playlist;
    }

    @Override
    public TrackInfo[] getAllTracks() throws BusException {
        final RemotePlayer remotePlayer = getRemotePlayer();

        TrackInfo[] allTracks = MusicUtils.getAllTracks(remotePlayer.mService.getApplicationContext());
        for ( int i = 0; i < allTracks.length; i++ ) {
            allTracks[i].mTrackUri = remotePlayer.mWebServer.getTrackUri(allTracks[i].mTrackId, false);
            allTracks[i].mDownloadUri = remotePlayer.mWebServer.getTrackUri(allTracks[i].mTrackId, true);
            allTracks[i].mTrackId = -1;
        }
        return allTracks;
    }

    @Override
    public String getServerIpAddress() throws BusException {
        return getRemotePlayer().mWebServer.getServerIpAddress();
    }

    @Override
    public int getServerPort() throws BusException {
        return getRemotePlayer().mWebServer.getServerPort();
    }
}
