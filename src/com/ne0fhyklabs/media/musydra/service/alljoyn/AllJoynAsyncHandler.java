package com.ne0fhyklabs.media.musydra.service.alljoyn;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.Status;
import org.alljoyn.cops.peergroupmanager.BusObjectData;
import org.alljoyn.cops.peergroupmanager.PeerGroupListener;
import org.alljoyn.cops.peergroupmanager.PeerGroupManager;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ne0fhyklabs.media.musydra.service.webserver.WebServer;
import com.ne0fhyklabs.media.musydra.utils.StateManager;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraState;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraStateKey;
import com.ne0fhyklabs.media.musydra.utils.TrackInfo;

public class AllJoynAsyncHandler extends Handler {

    private final static String TAG = AllJoynAsyncHandler.class.getSimpleName();

    /**
     * The object path used to identify the service "location" in the bus attachment.
     */
    static final String OBJECT_PATH = "/musydraShare";

    /**
     * AllJoyn functionality
     */
    static final int EXIT = 1;
    static final int CREATE_GROUP = 2;
    static final int DESTROY_GROUP = 3;
    static final int JOIN_GROUP = 4;
    static final int LEAVE_GROUP = 5;

    /**
     * Web server calls
     */
    static final int UPDATE_SERVER_TIME_DIFF = 6;

    /**
     * AllJoyn MultiPlayer signals
     */
    static final int MULTIPLAYER_SETDATASOURCE = 12;
    static final int MULTIPLAYER_START = 13;
    static final int MULTIPLAYER_PAUSE = 14;
    static final int MULTIPLAYER_STOP = 15;
    static final int MULTIPLAYER_SETVOLUME = 16;
    static final int MULTIPLAYER_SEEK = 17;

    /**
     * AllJoyn two way method calls
     */
    static final int TWO_WAY_STATUS_REFRESH = 18;
    static final int PING_GROUP = 19;

    private static final int PING_GROUP_PERIOD = 60 * 1000; // 1 min in milliseconds
    /**
     * AllJoyn preferences keys, and default values
     */
    public static final String PREF_PEER_GROUP_NOTIFICATION = "alljoynClientNotificationFlag";
    public static final boolean DEFAULT_PEER_GROUP_NOTIFICATION = true;

    public static final String PREF_AUTO_CONNECT_LIST = "auto_connect_list";

    // AllJoyn object
    private PeerGroupManager mPgm;

    private PeerGroupListener mPgmListener;

    /**
     * Object whose methods are the bus signal handlers
     */
    private MusydraSignalHandler mSignalHandler;

    /**
     * The MusydraService is the instance of an AllJoyn interface that is exported on the bus and
     * allow to send signals implementing messages
     */
    private MusydraBusObject mBusObject;

    /**
     * Service RemotePlayerInterface object Used to invoke alljoyn signals
     */
    private RemotePlayerInterface mSignalEmitter;
    RemotePlayerInterface mRemoteObjInterface;

    private HashSet<String> mAutoConnectList;

    private final WeakReference<RemotePlayer> mRemotePlayerReference;

    public AllJoynAsyncHandler(final RemotePlayer remotePlayer, final Looper looper) {
        super(looper);
        mRemotePlayerReference = new WeakReference<RemotePlayer>(remotePlayer);

        // Load the auto connect list.
        loadAutoConnectList();

        // Create the peer group listener
        mPgmListener = new RemotePlayerPeerGroupListener();

        // Create the signal handler object
        mSignalHandler = new MusydraSignalHandler(remotePlayer);

        mBusObject = new MusydraBusObject(remotePlayer);
        ArrayList<BusObjectData> busObjects = new ArrayList<BusObjectData>();
        busObjects.add(new BusObjectData(mBusObject, OBJECT_PATH));

        mPgm = new PeerGroupManager(remotePlayer.mService.getApplicationContext(), RemotePlayer.GROUP_PREFIX,
                                    mPgmListener, busObjects);
        Status status = mPgm.registerSignalHandlers(mSignalHandler);
        if ( status != Status.OK ) {
            throw new IllegalStateException("Failed to register signal handlers - " + status);
        }
    }

    private void saveAutoConnectList() {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(getRemotePlayer().mService);
        settings.edit().putStringSet(PREF_AUTO_CONNECT_LIST, mAutoConnectList).commit();
    }

    private void loadAutoConnectList() {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(getRemotePlayer().mService);

        mAutoConnectList = new HashSet<String>(settings.getStringSet(PREF_AUTO_CONNECT_LIST,
                                                                     new HashSet<String>()));
    }

    public void addToAutoConnect(String peerGroupName) {
        mAutoConnectList.add(peerGroupName);

        if ( !hasJoinedPeerGroup() && getFoundPeerGroups().contains(peerGroupName) )
            doJoinGroup(getRemotePlayer(), peerGroupName);

        saveAutoConnectList();
    }

    public void removeFromAutoConnect(String peerGroupName) {
        mAutoConnectList.remove(peerGroupName);
        saveAutoConnectList();
    }

    public String[] getAutoConnectList() {
        return mAutoConnectList.toArray(new String[0]);
    }

    public void destroy() {
        saveAutoConnectList();

        removeCallbacksAndMessages(null);
        getLooper().quit();

        if ( mPgm != null )
            mPgm.cleanup();

        mRemotePlayerReference.clear();
    }

    protected RemotePlayer getRemotePlayer() {
        RemotePlayer remotePlayer = mRemotePlayerReference.get();
        if ( remotePlayer == null )
            throw new IllegalStateException("Weak reference to remote player was cleaned.");

        return remotePlayer;
    }

    ArrayList<String> getFoundPeerGroups() {
        if ( mPgm == null )
            return new ArrayList<String>(0);

        return mPgm.listFoundGroups();
    }

    boolean hasJoinedPeerGroup() {
        if ( mPgm == null )
            return false;

        return !mPgm.listJoinedGroups().isEmpty();
    }

    ArrayList<String> getJoinedPeerGroups() {
        if ( mPgm == null )
            return new ArrayList<String>();

        return mPgm.listJoinedGroups();
    }

    boolean isHostingGroup() {
        if ( mPgm == null )
            return false;

        return !mPgm.listHostedGroups().isEmpty();
    }

    ArrayList<String> getHostedGroups() {
        if ( mPgm == null )
            return new ArrayList<String>();

        return mPgm.listHostedGroups();
    }

    /**
     * Message handler for the worker thread that handles background tasks for the AllJoyn bus.
     */
    @Override
    public void handleMessage(final Message msg) {
        final RemotePlayer remotePlayer = getRemotePlayer();

        switch (msg.what) {

            /* AllJoyn functionality */
            case CREATE_GROUP: {
                final String groupName = (String) msg.obj;
                doCreateGroup(remotePlayer, groupName);
                break;
            }
            case DESTROY_GROUP: {
                final String groupName = (String) msg.obj;
                doDestroyGroup(remotePlayer, groupName);
                break;
            }

            case JOIN_GROUP: {
                final String peerGroupName = (String) msg.obj;
                doJoinGroup(remotePlayer, peerGroupName);
                break;
            }
            case LEAVE_GROUP: {
                final String peerGroupName = (String) msg.obj;
                doLeaveGroup(remotePlayer, peerGroupName);
                break;
            }

            case EXIT:
                destroy();
                break;

                /*
                 * Web server functionality
                 */
            case UPDATE_SERVER_TIME_DIFF:
                doUpdateServerTimeDiff(remotePlayer);
                break;

                /*
                 * Remote player alljoyn signals
                 */
            case MULTIPLAYER_PAUSE:
            case MULTIPLAYER_SEEK:
            case MULTIPLAYER_SETDATASOURCE:
            case MULTIPLAYER_SETVOLUME:
            case MULTIPLAYER_START:
            case MULTIPLAYER_STOP:
                doRemoteSignal(remotePlayer, msg.what, msg.obj);
                break;

                /*
                 * Remote player alljoyn method calls
                 */
            case TWO_WAY_STATUS_REFRESH:
                doRemoteMethod(remotePlayer, msg.what, msg.obj);
                break;

            case PING_GROUP:{
                final String peerGroupName = (String) msg.obj;
                doPingGroup(remotePlayer, peerGroupName);
                break;
            }

            default:
                break;
        }
    }

    /** AllJoyn functionality implementations */

    /**
     * Create and host a group for sharing music from.
     * Can only be called when the alljoyn module is not connected to any other peer group.
     */
    private void doCreateGroup(final RemotePlayer remotePlayer, final String groupName) {
        final String methodName = "doCreateGroup(" + groupName + ")";
        Log.i(TAG, methodName);

        if ( hasJoinedPeerGroup() ) {
            Log.w(TAG, methodName + " - Operation aborted. Module has already joined another peer group.");
            return;
        }

        ArrayList<String> hostedGroupList = mPgm.listHostedGroups();

        // Check if the group already exists.
        if ( hostedGroupList.contains(groupName) ) {
            Log.w(TAG, methodName + " - Operation aborted. Group was already created.");
            return;
        }

        // Check if other groups are already hosted. If so, destroy them.
        if ( hostedGroupList.size() > 0 ) {
            for ( String hostedGroup : hostedGroupList )
                doDestroyGroup(remotePlayer, hostedGroup);
        }

        Status status = mPgm.createGroup(groupName);
        if ( status == Status.OK ) {
            Bundle groupInfo = new Bundle(1);
            groupInfo.putString(RemotePlayer.PREF_GROUP_NAME, groupName);
            remotePlayer.setState(MusydraState.ALLJOYN_HOST_GROUP_CREATED, groupInfo);

            // Activate the webserver
            if ( !remotePlayer.mWebServer.isRunning() )
                remotePlayer.mWebServer.start();
        }
        else {
            Log.e(TAG, methodName + ": Unable to create peer group - " + status);
        }
    }

    /**
     * Cancel an advertisement on an AllJoyn bus attachment.
     */
    private void doDestroyGroup(final RemotePlayer remotePlayer, final String groupName) {
        final String methodName = "doDestroyGroup(" + groupName + ")";
        Log.i(TAG, methodName);

        // Check if the group exists
        if ( !mPgm.listHostedGroups().contains(groupName) ) {
            Log.w(TAG, methodName + " - Operation aborted. Hosted group doesn't exist.");
            return;
        }

        // Notify whoever is connected, that this group is being destroyed
        if ( mSignalEmitter != null )
            try {
                mSignalEmitter.disconnect(groupName);
            } catch (BusException e) {
                Log.e(TAG, "Unable to notify connected client.", e);
            }

        Status status = mPgm.destroyGroup(groupName);
        if ( status == Status.OK ) {
            Bundle groupInfo = new Bundle(1);
            groupInfo.putString(RemotePlayer.PREF_GROUP_NAME, groupName);
            remotePlayer.setState(MusydraState.ALLJOYN_IDLE, groupInfo);

            // Disable the signal emitter
            mSignalEmitter = null;

            // Stop the web server
            if ( remotePlayer.mWebServer.isRunning() )
                remotePlayer.mWebServer.stop();
        }
        else {
            Log.e(TAG, methodName + ": Unable to destroy hosted group - " + status);
        }

    }

    /**
     * Get the connected server time diff
     */
    private void doUpdateServerTimeDiff(final RemotePlayer remotePlayer) {
        // Clean any other msg requests
        removeMessages(UPDATE_SERVER_TIME_DIFF);

        // Check if the client is connected to an alljoyn server
        if ( !hasJoinedPeerGroup() ) {
            // Set the time diff back to 0
            remotePlayer.mServerTimeDiff.set(0);
            return;
        }

        // Check if the remote obj interface is set
        if ( mRemoteObjInterface == null )
            return;

        // Retrieve the remote server address, and port.
        try {
            String serverAddress = mRemoteObjInterface.getServerIpAddress();
            int serverPort = mRemoteObjInterface.getServerPort();
            URL serverUrl = new URL("http://" + serverAddress + ":" + serverPort);

            long serverTimestamp = WebServer.getServerTimestamp(serverUrl);
            long timeDiff = System.currentTimeMillis() - serverTimestamp;
            remotePlayer.mServerTimeDiff.set(timeDiff);

            Log.d(TAG, "Server time diff: " + timeDiff);

            // Reschedule the operation
            sendEmptyMessageDelayed(UPDATE_SERVER_TIME_DIFF, RemotePlayer.DELAY_SERVER_TIME_DIFF_UPDATE);
        } catch (Exception e) {
            Log.e(TAG, "Unable to retrieve alljoyn server ip address, and port. Retrying attempt shortly.", e);
            sendEmptyMessageDelayed(UPDATE_SERVER_TIME_DIFF, RemotePlayer.DELAY_FAST_SERVER_TIME_DIFF_UPDATE);
        }
    }

    private void doPingGroup(final RemotePlayer remotePlayer, final String groupName) {
        // Clean any other msg requests
        removeMessages(PING_GROUP);

        // Check if the client is connect
        if ( !hasJoinedPeerGroup() )
            return;

        // Check if this client is connected to the group name
        if ( mPgm.listJoinedGroups().contains(groupName) ) {

            // Check if the remote obj interface is set
            if ( mRemoteObjInterface == null ) {
                doLeaveGroup(remotePlayer, groupName);
                return;
            }

            try {
                String pingedGroupName = mRemoteObjInterface.ping();
                Log.d(TAG, "Group ( " + pingedGroupName + " ) is still up.");
            } catch (BusException e) {
                Log.w(TAG, "Unable to reach group. It's probably gone. Disconnecting...", e);
                doLeaveGroup(remotePlayer, groupName);
                return;
            }
        }

        // Schedule another check in PING_GROUP_PERIOD
        Message msg = obtainMessage(PING_GROUP, groupName);
        sendMessageDelayed(msg, PING_GROUP_PERIOD);
    }

    /**
     * Join an existing remote session.
     */
    private void doJoinGroup(final RemotePlayer remotePlayer, final String peerGroupName) {
        final String methodName = "doJoinGroup(" + peerGroupName + ")";
        Log.i(TAG, methodName);

        final ArrayList<String> joinedPeerGroups = mPgm.listJoinedGroups();

        // Check if this peer group is already joined
        if ( joinedPeerGroups.contains(peerGroupName) ) {
            Log.w(TAG, methodName + " - Operation aborted. Peer group was already joined.");
            return;
        }

        // Check if any other peer group is already joined. If so, leave the other group(s) to join
        // this one.
        if ( joinedPeerGroups.size() > 0 ) {
            for ( String joinedGroup : joinedPeerGroups )
                doLeaveGroup(remotePlayer, joinedGroup);
        }

        Status status = mPgm.joinGroup(peerGroupName);
        if ( status == Status.OK ) {
            Log.i(TAG, methodName + " - Operation successful.");

            // Disable hosted group
            ArrayList<String> hostedGroupList = mPgm.listHostedGroups();
            if ( hostedGroupList.size() > 0 ) {
                for ( String hostedGroup : hostedGroupList )
                    doDestroyGroup(remotePlayer, hostedGroup);
            }

            // Retrieve the proxy object
            Log.i(TAG, methodName + " - Establishing remote interface object.");
            mRemoteObjInterface = mPgm.getRemoteObjectInterface(mPgm.getGroupHostPeerId(peerGroupName),
                                                                peerGroupName, OBJECT_PATH,
                                                                RemotePlayerInterface.class);

            // Get the server time
            doUpdateServerTimeDiff(remotePlayer);

            // Ping the group to see if it's there.
            doPingGroup(remotePlayer, peerGroupName);

            Bundle joinedGroupInfo = new Bundle(1);
            joinedGroupInfo.putString(StateManager.KEY_PEER_GROUP_NAME, peerGroupName);
            remotePlayer.setState(MusydraState.ALLJOYN_PEER_GROUP_JOINED, joinedGroupInfo);

            // Refresh media player status
            remotePlayer.refreshMediaStatusOnConnection();
        }
        else {
            Log.e(TAG, methodName + " - Operation failed - " + status);
            remotePlayer.mService.mStateManager.notify("Unable to connect to " + peerGroupName);

            // Broadcast the change
            remotePlayer.mService.sendBroadcast(new Intent(RemotePlayer.ACTION_PEER_GROUPS_UPDATE));
        }
    }

    /**
     * Leave the currently joined remote session
     */
    private void doLeaveGroup(final RemotePlayer remotePlayer, final String peerGroupName) {
        final String methodName = "doLeaveGroup(" + peerGroupName + ")";
        Log.i(TAG, methodName);

        // Check if we've joined this peer group
        if ( !mPgm.listJoinedGroups().contains(peerGroupName) ) {
            Log.w(TAG, methodName + " - Operation aborted, peer group was not joined.");
            return;
        }

        Status status = mPgm.leaveGroup(peerGroupName);
        if ( status == Status.OK ) {
            Log.i(TAG, methodName + " - Operation successful.");

            sessionDisconnectCleaner(remotePlayer, peerGroupName);
        }
        else {
            Log.e(TAG, methodName + " - Operation failed - " + status);
        }
    }

    private void sessionDisconnectCleaner(final RemotePlayer remotePlayer, final String peerGroupName) {
        // Set the state
        Bundle peerGroupInfo = new Bundle(1);
        peerGroupInfo.putString(StateManager.KEY_PEER_GROUP_NAME, peerGroupName);
        remotePlayer.setState(MusydraState.ALLJOYN_IDLE, peerGroupInfo);

        // Disable the remote object interface
        mRemoteObjInterface = null;

        // Update the server time diff
        doUpdateServerTimeDiff(remotePlayer);

        // Restart any hosted group that was suspended.
        if ( remotePlayer.isSharingActive() ) {
            doCreateGroup(remotePlayer, remotePlayer.getGroupName());
        }
    }

    /**
     * Remote player alljoyn signals
     */
    private void doRemoteSignal(final RemotePlayer remotePlayer, int commandId, Object arg1) {
        if ( mSignalEmitter == null ) {
            Log.e(TAG, "doRemoteSignal(" + commandId + ", " + arg1
                  + ") - Signal emitter handle has not been initialized.");
            return;
        }

        try {
            switch (commandId) {
                case MULTIPLAYER_PAUSE: {
                    long eventTime = (Long) arg1;
                    mSignalEmitter.pause(eventTime);
                    break;
                }

                case MULTIPLAYER_SEEK: {
                    long[] timeAndPosition = (long[]) arg1;
                    mSignalEmitter.seek(timeAndPosition[0], timeAndPosition[1]);
                    break;
                }

                case MULTIPLAYER_SETDATASOURCE:
                    TrackInfo trackInfo = (TrackInfo) arg1;
                    // Remove the id column, and generate the remoteUri
                    // for streaming if needed
                    trackInfo.mTrackUri = remotePlayer.mWebServer.getTrackUri(trackInfo.mTrackId, false);
                    trackInfo.mDownloadUri = remotePlayer.mWebServer.getTrackUri(trackInfo.mTrackId, true);
                    trackInfo.mTrackId = -1;

                    mSignalEmitter.setDataSource(trackInfo);
                    break;

                case MULTIPLAYER_SETVOLUME:
                    float vol = (Float) arg1;
                    mSignalEmitter.setVolume(vol);
                    break;

                case MULTIPLAYER_START: {
                    long eventTime = (Long) arg1;
                    mSignalEmitter.start(eventTime);
                    break;
                }

                case MULTIPLAYER_STOP:
                    mSignalEmitter.stop();
                    break;
            }
        } catch (BusException e) {
            Log.e(TAG, "doRemoteSignal(" + commandId + ", " + arg1 + ") - Bus exception: " + e.getMessage());
        }
    }

    private void doRemoteMethod(final RemotePlayer remotePlayer, int methodId, Object arg1) {
        if ( mRemoteObjInterface == null ) {
            Log.e(TAG, "doRemoteMethod(): Remote object has not been initialized.");
            return;
        }

        try {
            switch (methodId) {

                case TWO_WAY_STATUS_REFRESH:
                    if ( mSignalHandler == null ) {
                        Log.e(TAG, "Signal handler has not been initialized.");
                        break;
                    }

                    // Retrieve the current track
                    TrackInfo currentRemoteTrack = mRemoteObjInterface.getCurrentTrack();
                    if ( currentRemoteTrack == null )
                        break;

                    mSignalHandler.stop();
                    mSignalHandler.setDataSource(currentRemoteTrack);
                    if ( remotePlayer.getState(MusydraStateKey.MULTI_PLAYER_STATE) != MusydraState.MULTI_PLAYER_PREPARED )
                        break;

                    // Retrieve the remote media player state
                    String stateName = mRemoteObjInterface.getMultiPlayerState();
                    MusydraState state = MusydraState.valueOf(stateName);

                    switch (state) {
                        case MULTI_PLAYER_STARTED:
                            // Start playing
                            // TODO: complete logic
                            long[] positionAndTime = mRemoteObjInterface.getCurrentTrackPosition();
                            mSignalHandler.start(positionAndTime[0]);
                            mSignalHandler.seek(positionAndTime[0], positionAndTime[1]);
                            break;

                        default:
                            break;
                    }
                    break;
            }
        } catch (BusException e) {
            Log.e(TAG, "doRemoteMethod() exception.", e);
        }
    }

    private class RemotePlayerPeerGroupListener extends PeerGroupListener {

        private boolean shouldNotify(Service service) {
            return PreferenceManager.getDefaultSharedPreferences(service)
                    .getBoolean(PREF_PEER_GROUP_NOTIFICATION, DEFAULT_PEER_GROUP_NOTIFICATION);
        }

        /**
         * Called when a new group advertisement is found. This will not be triggered for your own
         * hosted groups.
         *
         * @param groupName
         *            the groupName that was found
         * @param transport
         *            the transport that the groupName was discovered on
         */
        @Override
        public void foundAdvertisedName(String groupName, short transport) {
            RemotePlayer remotePlayer = getRemotePlayer();

            Log.i(TAG, "PeerGroupListener.foundAdvertisedName(" + groupName + ", " + transport + ")");

            if ( shouldNotify(remotePlayer.mService) ) {
                remotePlayer.mService.mStateManager.notify(StateManager.ALLJOYN_FOUND_PEER_NOTIFICATION_ID,
                        "Musydra streams in range!");
            }

            // Broadcast the change
            remotePlayer.mService
            .sendBroadcast(new Intent(RemotePlayer.ACTION_PEER_GROUPS_UPDATE));

            // Join a group automatically if it's part of the auto connect list.
            if ( mAutoConnectList.contains(groupName) && !hasJoinedPeerGroup() ) {
                doJoinGroup(remotePlayer, groupName);
            }
        }

        /**
         * Called when a group that was previously reported through foundAdvertisedName has become
         * unavailable. This will not be triggered for your own hosted groups.
         *
         * @param groupName
         *            the group name that has become unavailable
         * @param transport
         *            the transport that stopped receiving the groupName advertisement
         */
        @Override
        public void lostAdvertisedName(String groupName, short transport) {
            RemotePlayer remotePlayer = getRemotePlayer();

            Log.i(TAG, "PeerGroupListener.lostAdvertisedName(" + groupName + ", " + transport + ")");

            if ( mPgm.listFoundGroups().isEmpty() && shouldNotify(remotePlayer.mService) ) {
                remotePlayer.mService.mStateManager
                .cancelNotification(StateManager.ALLJOYN_FOUND_PEER_NOTIFICATION_ID);
            }

            // If we've joined this group, check to see if it's still around
            if ( mPgm.listJoinedGroups().contains(groupName) ) {
                doPingGroup(remotePlayer, groupName);
            }

            // Broadcast the change
            remotePlayer.mService
            .sendBroadcast(new Intent(RemotePlayer.ACTION_PEER_GROUPS_UPDATE));
        }

        /**
         * Called when a group becomes disconnected.
         *
         * @param groupName
         *            the group that became disconnected
         */
        @Override
        public void groupLost(String groupName) {
            Log.i(TAG, "PeerGroupListener.groupLost(" + groupName + ")");

            if ( mPgm.listHostedGroups().contains(groupName) ) {
                mSignalEmitter = null;
            }

            // Broadcast the change
            getRemotePlayer().mService.sendBroadcast(new Intent(
                                                                RemotePlayer.ACTION_PEER_GROUPS_UPDATE));
        }

        /**
         * Called when a new peer joins a group.
         *
         * @param peerId
         *            the id of the peer that joined
         * @param groupName
         *            the group that the peer joined
         * @param numPeers
         *            the current number of peers in the group
         */
        @Override
        public void peerAdded(String peerId, String groupName, int numPeers) {
            Log.i(TAG, "PeerGroupListener.peerAdded(" + peerId + ", " + groupName + ", " + numPeers + ")");

            if ( mPgm.listHostedGroups().contains(groupName) ) {
                if ( mSignalEmitter == null ) {
                    // Set the signal interface
                    mSignalEmitter = mPgm.getSignalInterface(groupName, mBusObject,
                                                             RemotePlayerInterface.class);
                }

                Bundle peerInfo = new Bundle(1);
                peerInfo.putInt(StateManager.KEY_NUM_PEERS, Math.max(0, numPeers - 1));
                getRemotePlayer().setState(MusydraState.ALLJOYN_HOST_GROUP_CREATED, peerInfo);
            }

        }

        /**
         * Called when a new peer leaves a group.
         *
         * @param peerId
         *            the id of the peer that left
         * @param groupName
         *            the group that the peer left
         * @param numPeers
         *            the current number of peers in the group
         */
        @Override
        public void peerRemoved(String peerId, String groupName, int numPeers) {
            Log.i(TAG, "PeerGroupListener.peerRemoved(" + peerId + ", " + groupName + ", " + numPeers + ")");

            if ( groupName == null || mPgm.listHostedGroups().contains(groupName) ) {
                Bundle peerInfo = new Bundle(1);
                peerInfo.putInt(StateManager.KEY_NUM_PEERS, Math.max(0, numPeers - 1));
                getRemotePlayer().setState(MusydraState.ALLJOYN_HOST_GROUP_CREATED, peerInfo);
            }
        }
    };

}
