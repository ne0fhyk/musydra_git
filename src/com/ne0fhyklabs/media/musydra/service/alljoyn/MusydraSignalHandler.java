package com.ne0fhyklabs.media.musydra.service.alljoyn;

import org.alljoyn.bus.annotation.BusSignalHandler;

import android.util.Log;

import com.ne0fhyklabs.media.musydra.service.MusydraService;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.utils.TrackInfo;

public class MusydraSignalHandler {

    private final static String TAG = MusydraUtils.APP_TAG + ".RemotePlayer.MusydraSignalHandler";

    private final RemotePlayer mRemotePlayer;

    public MusydraSignalHandler(RemotePlayer remotePlayer) {
        mRemotePlayer = remotePlayer;
    }

    private boolean isInDJMode() {
        return MusydraUtils.getDJMode(mRemotePlayer.mService.getApplicationContext());
    }

    @BusSignalHandler(iface = "com.ne0fhyklabs.media.musydra.service.alljoyn", signal = "setDataSource")
    public void setDataSource(TrackInfo mediaInfo) {
        if ( !isInDJMode() )
            return;

        mRemotePlayer.mService.mTrackToPlay = mediaInfo;
        mRemotePlayer.mPlayer.setDataSource(mediaInfo);
    }

    @BusSignalHandler(iface = "com.ne0fhyklabs.media.musydra.service.alljoyn", signal = "start")
    public void start(long eventTime) {
        if ( !isInDJMode() )
            return;

        mRemotePlayer.mPlayer.start();

        long eventLagTime = mRemotePlayer.getLagCorrectionTime(eventTime);
        long currentPosition = mRemotePlayer.position();
        mRemotePlayer.mPlayer.seek(currentPosition + eventLagTime);

        mRemotePlayer.mService.notifyChange(MusydraService.META_CHANGED);
    }

    @BusSignalHandler(iface = "com.ne0fhyklabs.media.musydra.service.alljoyn", signal = "stop")
    public void stop() {
        if ( !isInDJMode() )
            return;

        mRemotePlayer.mPlayer.stop();
    }

    @BusSignalHandler(iface = "com.ne0fhyklabs.media.musydra.service.alljoyn", signal = "pause")
    public void pause(long eventTime) {
        if ( !isInDJMode() )
            return;

        mRemotePlayer.mPlayer.pause();

        long eventLagTime = mRemotePlayer.getLagCorrectionTime(eventTime);
        long currentPosition = mRemotePlayer.position();
        mRemotePlayer.mPlayer.seek(currentPosition + eventLagTime);

    }

    @BusSignalHandler(iface = "com.ne0fhyklabs.media.musydra.service.alljoyn", signal = "seek")
    public void seek(long eventTime, long whereto) {
        if ( !isInDJMode() )
            return;

        long eventLagTime = mRemotePlayer.getLagCorrectionTime(eventTime);
        mRemotePlayer.mPlayer.seek(whereto + eventLagTime);
    }

    @BusSignalHandler(iface = "com.ne0fhyklabs.media.musydra.service.alljoyn", signal = "setVolume")
    public void setVolume(float vol) {
        if ( !isInDJMode() )
            return;

        mRemotePlayer.mPlayer.setVolume(vol);
    }

    @BusSignalHandler(iface = "com.ne0fhyklabs.media.musydra.service.alljoyn", signal = "disconnect")
    public void disconnect(String groupName) {
        Log.i(TAG, "Disconnecting from " + groupName);
        mRemotePlayer.leavePeerGroup(groupName);
    }

    public long duration() {
        return mRemotePlayer.mPlayer.duration();
    }
}