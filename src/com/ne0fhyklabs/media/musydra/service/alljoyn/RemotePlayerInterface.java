package com.ne0fhyklabs.media.musydra.service.alljoyn;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusMethod;
import org.alljoyn.bus.annotation.BusSignal;

import com.ne0fhyklabs.media.musydra.utils.TrackInfo;

/**
 * The BusInterface annotation is used to tell the code that this interface is
 * an AllJoyn interface.
 * 
 * The 'name' value is used to specify by which name this interface will be
 * known. It defaults to the fully qualified name of the java interface.
 * 
 * 
 * The BusProperty annotation signifies that this function should be used as
 * part of the AllJoyn interface. A BusProperty always starts with get or set.
 * The set method always takes a single value, while the get method always
 * returns a single value.
 * 
 * All methods that use the BusProperty annotation can throw a BusException, and
 * should indicate this fact
 * 
 * 
 * The BusSignal annotation signifies that this function is used as a signal
 * emitter, only called to send signals from the service to the clients.
 * 
 * @author fhuya
 * 
 */
@BusInterface(name = "com.ne0fhyklabs.media.musydra.service.alljoyn")
public interface RemotePlayerInterface {

    @BusSignal(signature = "r")
    public void setDataSource(TrackInfo mediaInfo) throws BusException;

    @BusSignal(signature = "x")
    public void start(long eventTime) throws BusException;

    @BusSignal(signature = "x")
    public void pause(long eventTime) throws BusException;

    @BusSignal
    public void stop() throws BusException;

    @BusSignal(signature = "d")
    public void setVolume(float vol) throws BusException;

    @BusSignal(signature = "xx")
    public void seek(long eventTime, long whereto) throws BusException;

    @BusSignal
    public void disconnect(String groupName) throws BusException;

    @BusMethod
    public String ping() throws BusException;

    @BusMethod
    public String getMultiPlayerState() throws BusException;

    @BusMethod(replySignature = "ax")
    public long[] getCurrentTrackPosition() throws BusException;

    @BusMethod(replySignature = "r")
    public TrackInfo getCurrentTrack() throws BusException;

    @BusMethod(replySignature = "ar")
    public TrackInfo[] getPlaylist() throws BusException;

    @BusMethod(replySignature = "ar")
    public TrackInfo[] getAllTracks() throws BusException;

    @BusMethod(replySignature = "s")
    public String getServerIpAddress() throws BusException;

    @BusMethod
    public int getServerPort() throws BusException;
}
