package com.ne0fhyklabs.media.musydra.service.alljoyn;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ne0fhyklabs.media.musydra.service.MultiPlayer;
import com.ne0fhyklabs.media.musydra.service.MusydraService;
import com.ne0fhyklabs.media.musydra.service.webserver.WebServer;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraState;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraStateKey;
import com.ne0fhyklabs.media.musydra.utils.TrackInfo;

public class RemotePlayer {
    private static final String TAG = MusydraUtils.APP_TAG + ".RemotePlayer";

    /**
     * Load the native alljoyn_java library. The actual AllJoyn code is written in C++ and the
     * alljoyn_java library provides the language bindings from Java to C++ and vice versa.
     */
    static {
        Log.i(TAG, "System.loadLibrary(\"alljoyn_java\")");
        System.loadLibrary("alljoyn_java");
    }

    /**
     * The well-knows name prefix which all bus attachments hosting a channel will use. The
     * NAME_PREFIX and the channel name are composed to give the well-known name a hosting bus
     * attachment will request and advertise.
     */
    public static final String GROUP_PREFIX = "com.ne0fhyklabs.media.musydra.service.alljoyn";

    public static final String PREF_GROUP_NAME = "alljoynServiceName";
    public static final String DEFAULT_GROUP_NAME = "musydra_share";

    /**
     * Intent filter's actions
     */
    public static final String ACTION_GROUP_NAME_UPDATE = "com.ne0fhyklabs.media.musydra.joynlocalhostchanged";
    public static final String ACTION_PEER_GROUPS_UPDATE = "com.ne0fhyklabs.media.musydra.joynchannelschanged";

    public static final String PREF_SHARING_ACTIVE = "alljoynServiceStart";
    public static final boolean DEFAULT_SHARING_ACTIVE = false;

    final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if ( MusydraUtils.ACTION_DJ_MODE_CHANGED.equals(action) ) {
                boolean djMode = intent.getBooleanExtra(MusydraUtils.KEY_DJ_MODE, false);
                if ( djMode )
                    refreshMediaStatusOnConnection();
            }
            else if ( Intent.ACTION_TIME_CHANGED.equals(action) ||
                      Intent.ACTION_TIMEZONE_CHANGED.equals(action) ) {
                updateServerTimeDiff();
            }
        }
    };

    /**
     * Objet used to interact with the media player.
     */
    MultiPlayer mPlayer;

    /**
     * The instance of the AllJoyn background thread handler. When the service exits, onDestroy()
     * spins down the thread
     */
    private AllJoynAsyncHandler mAsyncHandler;

    /**
     * Keep track of the time difference between the alljoyn server, and this client.
     */
    AtomicLong mServerTimeDiff = new AtomicLong(0);

    /**
     * How long before updating the server time difference
     */
    static long DELAY_SERVER_TIME_DIFF_UPDATE = 10 * 60 * 1000; // 10 min in milliseconds
    static long DELAY_FAST_SERVER_TIME_DIFF_UPDATE = 1 * 60 * 1000; // 1 min in milliseconds

    MusydraService mService;

    /**
     * Web server used to stream, or sync files when the device is broadcasting
     */
    WebServer mWebServer;

    public RemotePlayer(MusydraService service) {
        mService = service;
    }

    /**
     * We spin up a background thread to handle any long lived requests (pretty much all AllJoyn
     * calls that involve communication with remote processes) that need to be done.
     */
    public void create() {
        Log.i(TAG, "create()");

        // Create the MultiPlayer object
        mPlayer = new MultiPlayer(mService);

        // Create the webserver
        mWebServer = new WebServer(mService.getApplicationContext());

        startBusThread();

        if ( isSharingActive() && !mAsyncHandler.hasJoinedPeerGroup() ) {
            createSharingGroup(getGroupName());
        }

        // Initialize and register the broadcast receiver
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MusydraUtils.ACTION_DJ_MODE_CHANGED);

        // Listening for 'wall' clock change so we can update the time difference
        // between an alljoyn server, and this client (if connected).
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);

        mService.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    /**
     * Order the background thread to exit.
     */
    public void release() {
        Log.i(TAG, "release()");

        mService.unregisterReceiver(mBroadcastReceiver);

        // Stop the webserver if it's still running
        if ( mWebServer != null && mWebServer.isRunning() )
            mWebServer.stop();

        if ( mAsyncHandler.hasJoinedPeerGroup() ) {
            leaveAllPeerGroup();
        }

        if ( mAsyncHandler.isHostingGroup() ) {
            destroyAllSharingGroup();
        }

        stopBusThread();

        // Release the media player
        mPlayer.release();
    }

    public void addToAutoConnectList(final String groupName) {
        mAsyncHandler.addToAutoConnect(groupName);
    }

    public void removeFromAutoConnectList(final String groupName) {
        mAsyncHandler.removeFromAutoConnect(groupName);
    }

    public String[] getAutoConnectList() {
        return mAsyncHandler.getAutoConnectList();
    }

    public String getGroupName() {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mService);
        return settings.getString(PREF_GROUP_NAME, DEFAULT_GROUP_NAME);
    }

    /**
     * Set the channel name used by the service to advertise itself
     *
     * @param name
     *            service channel name
     */
    public void setGroupName(final String groupName) {
        if ( groupName == null )
            return;

        if ( isSharingActive() && mAsyncHandler.isHostingGroup() ) {
            createSharingGroup(groupName);
        }

        mService.sendBroadcast(new Intent(ACTION_GROUP_NAME_UPDATE));
    }

    /**
     * Tear down the thread that's servicing long-lived remote operation
     */
    private void stopBusThread() {
        exit();
        mAsyncHandler = null;
    }

    private void startBusThread() {
        HandlerThread busThread = new HandlerThread("Async Handler Thread");
        busThread.start();

        mAsyncHandler = new AllJoynAsyncHandler(this, busThread.getLooper());
    }

    void refreshMediaStatusOnConnection() {
        mAsyncHandler.sendEmptyMessage(AllJoynAsyncHandler.TWO_WAY_STATUS_REFRESH);
    }

    public boolean isSharingActive() {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mService);
        return settings.getBoolean(RemotePlayer.PREF_SHARING_ACTIVE, DEFAULT_SHARING_ACTIVE);
    }

    public void setSharingActive(boolean isActive) {
        final String groupName = getGroupName();

        if ( isActive ) {
            createSharingGroup(groupName);
        }
        else {
            destroySharingGroup(groupName);
        }

        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mService);
        if ( settings.getBoolean(PREF_SHARING_ACTIVE, DEFAULT_SHARING_ACTIVE) != isActive ) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(PREF_SHARING_ACTIVE, isActive);
            editor.commit();
        }
    }

    public RemotePlayerInterface getRemoteObjInterface() {
        return mAsyncHandler.mRemoteObjInterface;
    }

    public String[] getFoundPeerGroupList() {
        return mAsyncHandler.getFoundPeerGroups().toArray(new String[0]);
    }

    /**
     * Get the service channel name the client is looking for via BusAttachment.findAdvertisedName
     *
     * @return
     */
    public ArrayList<String> getJoinedPeerGroups() {
        return mAsyncHandler.getJoinedPeerGroups();
    }

    long getLagCorrectionTime(long eventServerTime) {
        // Transform to local time using the server time difference
        long eventLocalTime = eventServerTime + mServerTimeDiff.get();

        // Take the difference between event local time, and the current time.
        long eventLagTime = System.currentTimeMillis() - eventLocalTime;
        Log.d(TAG, "Event lag time: " + eventLagTime);

        return eventLagTime;
    }

    /**
     * Update the server time diff (if connected)
     */
    private void updateServerTimeDiff() {
        Log.i(TAG, "updateServerTimeDiff()");
        mAsyncHandler.sendEmptyMessage(AllJoynAsyncHandler.UPDATE_SERVER_TIME_DIFF);
    }

    /**
     * Exit the background handler thread. This will be the last message executed by an instance of
     * the handler
     */
    public void exit() {
        Log.i(TAG, "exit()");
        mAsyncHandler.sendEmptyMessage(AllJoynAsyncHandler.EXIT);
    }

    public void createSharingGroup(final String groupName) {
        Log.i(TAG, "createSharingGroup()");
        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.CREATE_GROUP, groupName);
        mAsyncHandler.sendMessage(msg);
    }

    public void destroySharingGroup(final String groupName) {
        Log.i(TAG, "destroySharingGroup()");
        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.DESTROY_GROUP, groupName);
        mAsyncHandler.sendMessage(msg);
    }

    public void destroyAllSharingGroup() {
        Log.i(TAG, "destroyAllSharingGroup()");

        ArrayList<String> hostedGroupList = mAsyncHandler.getHostedGroups();
        for ( String hostedGroup : hostedGroupList )
            destroySharingGroup(hostedGroup);
    }

    public void joinPeerGroup(final String peerGroupName) {
        Log.i(TAG, "joinPeerGroup()");
        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.JOIN_GROUP, peerGroupName);
        mAsyncHandler.sendMessage(msg);
    }

    public void leavePeerGroup(final String peerGroupName) {
        Log.i(TAG, "leavePeerGroup()");
        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.LEAVE_GROUP, peerGroupName);
        mAsyncHandler.sendMessage(msg);
    }

    public void leaveAllPeerGroup() {
        Log.i(TAG, "leaveAllPeerGroup()");

        ArrayList<String> joinedPeerGroupList = mAsyncHandler.getJoinedPeerGroups();
        for ( String joinedPeerGroup : joinedPeerGroupList )
            leavePeerGroup(joinedPeerGroup);
    }

    /**
     * MultiPlayer methods mirroring + alljoyn functionalities where applicable
     */
    public void setAudioManagerComponents(AudioManager audioManager,
                                          OnAudioFocusChangeListener audioFocusChangeListener) {
        mPlayer.setAudioManagerComponents(audioManager, audioFocusChangeListener);
    }

    public void setDataSource(TrackInfo mediaInfo) {
        Log.i(TAG, "setDataSource(" + mediaInfo + ")");

        // Local command, disable dj mode
        MusydraUtils.setDJMode(mService.getApplicationContext(), false);

        TrackInfo remoteTrackInfo = TrackInfo.copyTrackInfo(mediaInfo);

        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.MULTIPLAYER_SETDATASOURCE,
                                                  remoteTrackInfo);
        mAsyncHandler.sendMessage(msg);

        mPlayer.setDataSource(mediaInfo);
    }

    public boolean isInitialized() {
        return mPlayer.isInitialized();
    }

    public void start() {
        Log.i(TAG, "start()");

        // Local command, disable dj mode
        MusydraUtils.setDJMode(mService.getApplicationContext(), false);

        Long eventTime = Long.valueOf(System.currentTimeMillis());
        mPlayer.start();

        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.MULTIPLAYER_START, eventTime);
        mAsyncHandler.sendMessage(msg);
    }

    public void pause() {
        Log.i(TAG, "pause()");

        // Local command, disable dj mode
        MusydraUtils.setDJMode(mService.getApplicationContext(), false);

        Long eventTime = Long.valueOf(System.currentTimeMillis());
        mPlayer.pause();

        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.MULTIPLAYER_PAUSE, eventTime);
        mAsyncHandler.sendMessage(msg);

    }

    public void stop() {
        Log.i(TAG, "stop()");

        // Local command, disable dj mode
        MusydraUtils.setDJMode(mService.getApplicationContext(), false);

        mPlayer.stop();

        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.MULTIPLAYER_STOP);
        mAsyncHandler.sendMessage(msg);
    }

    public void setHandler(Handler handler) {
        mPlayer.setHandler(handler);
    }

    public void setWakeLock(WakeLock wakeLock) {
        mPlayer.setWakeLock(wakeLock);
    }

    public long duration() {
        return mPlayer.duration();
    }

    public long position() {
        return mPlayer.position();
    }

    public void setVolume(float vol) {
        Log.i(TAG, "setVolume(" + vol + ")");

        mPlayer.setVolume(vol);

        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.MULTIPLAYER_SETVOLUME, vol);
        mAsyncHandler.sendMessage(msg);
    }

    public long seek(long whereto) {
        Log.i(TAG, "seek(" + whereto + ")");

        // Local command, disable dj mode
        MusydraUtils.setDJMode(mService.getApplicationContext(), false);

        long[] timeAndPosition = { System.currentTimeMillis(), whereto };
        mPlayer.seek(whereto);

        Message msg = mAsyncHandler.obtainMessage(AllJoynAsyncHandler.MULTIPLAYER_SEEK, timeAndPosition);
        mAsyncHandler.sendMessage(msg);

        return whereto;
    }

    public void setAudioSessionId(int sessionId) {
        mPlayer.setAudioSessionId(sessionId);
    }

    public int getAudioSessionId() {
        return mPlayer.getAudioSessionId();
    }

    MusydraStateKey setState(MusydraState state, Bundle props) {
        return mService.mStateManager.setState(state, props);
    }

    MusydraState getState(MusydraStateKey stateKey) {
        return mService.mStateManager.getState(stateKey);
    }
}
