package com.ne0fhyklabs.media.musydra.service;

import java.io.IOException;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.audiofx.AudioEffect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.MediaStore;
import android.util.Log;

import com.ne0fhyklabs.media.musydra.utils.StateManager;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraState;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraStateKey;
import com.ne0fhyklabs.media.musydra.utils.TrackInfo;
import com.ne0fhyklabs.media.musydra.utils.TrackInfoParcelable;

/**
 * Provides a unified interface for dealing with midi files and other media
 * files.
 */
public class MultiPlayer {

    private final String TAG = "Musydra.MultiPlayer";

    private ContentResolver mContentResolver;

    private MediaPlayer mMediaPlayer = new MediaPlayer();

    private Handler mHandler;
    private MusydraService mService;
    private WakeLock mWakeLock;

    private boolean mIsInitialized = false;

    private AudioManager mAudioManager;
    private OnAudioFocusChangeListener mAudioFocusListener;

    public MultiPlayer(MusydraService service) {
        mService = service;
        mMediaPlayer.setWakeMode(mService, PowerManager.PARTIAL_WAKE_LOCK);

        mContentResolver = mService.getContentResolver();
    }

    public MusydraState getState() {
        return mService.mStateManager.getState(MusydraStateKey.MULTI_PLAYER_STATE);
    }

    public MusydraStateKey setState(MusydraState state, Bundle props) {
        return mService.mStateManager.setState(state, props);
    }

    public void setAudioManagerComponents(AudioManager audioManager,
                                          OnAudioFocusChangeListener audioFocusChangeListener) {
        mAudioManager = audioManager;
        mAudioFocusListener = audioFocusChangeListener;
    }

    public void setDataSource(TrackInfo mediaInfo) {
        try {

            long id = mediaInfo.mTrackId;
            if ( id == -1 ) {
                // Grab the id of the media to play
                String[] projection = new String[] { MediaStore.Audio.Media._ID };
                String selections = MediaStore.Audio.Media.ARTIST + "= ? AND " +
                        MediaStore.Audio.Media.ALBUM + "= ? AND " + MediaStore.Audio.Media.TITLE +
                        "= ?";

                String[] selectionArgs = new String[] { mediaInfo.mArtist, mediaInfo.mAlbum, mediaInfo.mTitle };

                Cursor cursor = mContentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                                       projection, selections, selectionArgs, null);

                if ( cursor != null ) {
                    if ( cursor.getCount() > 0 ) {
                        cursor.moveToFirst();
                        id = cursor.getLong(0);
                    }
                    cursor.close();
                }
            }

            String path = null;
            if ( id != -1 )
                path = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/" + id;
            else
                path = mediaInfo.mTrackUri;

            mMediaPlayer.reset();
            mMediaPlayer.setOnPreparedListener(null);
            if ( path.startsWith("content://") ) {
                mMediaPlayer.setDataSource(mService, Uri.parse(path));
            }
            else {
                mMediaPlayer.setDataSource(path);
            }
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.prepare();
        } catch (IOException ex) {
            mIsInitialized = false;
            return;
        } catch (IllegalArgumentException ex) {
            mIsInitialized = false;
            return;
        }
        mMediaPlayer.setOnCompletionListener(listener);
        mMediaPlayer.setOnErrorListener(errorListener);
        Intent i = new Intent(AudioEffect.ACTION_OPEN_AUDIO_EFFECT_CONTROL_SESSION);
        i.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, getAudioSessionId());
        i.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, mService.getPackageName());
        mService.sendBroadcast(i);
        mIsInitialized = true;

        TrackInfoParcelable mediaInfoContainer = new TrackInfoParcelable(mediaInfo);
        Bundle props = new Bundle(1);
        props.putParcelable(StateManager.KEY_MEDIA_PROPS, mediaInfoContainer);
        setState(MusydraState.MULTI_PLAYER_PREPARED, props);
    }

    public boolean isInitialized() {
        return mIsInitialized;
    }

    public void start() {
        if ( getState() == MusydraState.MULTI_PLAYER_IDLE ) {
            Log.e(TAG, "start() - Multi Player is in an invalid state: " + MusydraState.MULTI_PLAYER_IDLE);
            return;
        }

        mAudioManager.requestAudioFocus(mAudioFocusListener, AudioManager.STREAM_MUSIC,
                                        AudioManager.AUDIOFOCUS_GAIN);
        mAudioManager.registerMediaButtonEventReceiver(new ComponentName(mService.getPackageName(),
                                                                         MediaButtonIntentReceiver.class
                                                                         .getName()));

        mMediaPlayer.start();

        if ( !mService.mIsSupposedToBePlaying )
            mService.mIsSupposedToBePlaying = true;

        setState(MusydraState.MULTI_PLAYER_STARTED, null);
    }

    public void stop() {
        mMediaPlayer.reset();
        mIsInitialized = false;

        if ( mService.mIsSupposedToBePlaying ) {
            mService.gotoIdleState();
            mService.mIsSupposedToBePlaying = false;
        }

        setState(MusydraState.MULTI_PLAYER_IDLE, null);
    }

    /**
     * You CANNOT use this player anymore after calling release()
     */
    public void release() {
        stop();
        mMediaPlayer.release();
        setState(MusydraState.MULTI_PLAYER_END, null);
    }

    public void pause() {
        MusydraState currentState = getState();
        if ( currentState != MusydraState.MULTI_PLAYER_STARTED &&
                currentState != MusydraState.MULTI_PLAYER_PAUSED ) {
            Log.e(TAG, "pause() - Multi Player is in an invalid state: " + currentState);
            return;
        }

        mMediaPlayer.pause();

        if ( mService.mIsSupposedToBePlaying ) {
            mService.gotoIdleState();
            mService.mIsSupposedToBePlaying = false;
        }

        setState(MusydraState.MULTI_PLAYER_PAUSED, null);
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
    }

    public void setWakeLock(WakeLock wakeLock) {
        mWakeLock = wakeLock;
    }

    MediaPlayer.OnCompletionListener listener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            // Acquire a temporary wake lock, since when we return from
            // this callback the MediaPlayer will release its wake lock
            // and allow the device to go to sleep.
            // This temporary wake lock is released when the RELEASE_WAKELOCK
            // message is processed, but just in case, put a timeout on it.
            if ( mService.mIsSupposedToBePlaying ) {
                mService.gotoIdleState();
                mService.mIsSupposedToBePlaying = false;
            }
            setState(MusydraState.MULTI_PLAYER_PLAYBACK_COMPLETED, null);
            mWakeLock.acquire(30000);
            mHandler.sendEmptyMessage(MusydraService.TRACK_ENDED);
            mHandler.sendEmptyMessage(MusydraService.RELEASE_WAKELOCK);
        }
    };

    MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            switch (what) {
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                    mIsInitialized = false;
                    mMediaPlayer.release();
                    // Creating a new MediaPlayer and settings its wake mode
                    // does not
                    // require the media service, so it's OK to do this now,
                    // while the
                    // service is still being restarted
                    mMediaPlayer = new MediaPlayer();
                    mMediaPlayer.setWakeMode(mService, PowerManager.PARTIAL_WAKE_LOCK);
                    mHandler.sendMessageDelayed(mHandler.obtainMessage(MusydraService.SERVER_DIED), 2000);
                    break;

                default:
                    Log.d("MultiPlayer", "Error: " + what + "," + extra);

                    mMediaPlayer.reset();
                    mIsInitialized = false;
                    break;
            }
            setState(MusydraState.MULTI_PLAYER_IDLE, null);
            return true;
        }
    };

    public long duration() {
        if ( getState() == MusydraState.MULTI_PLAYER_IDLE ) {
            Log.e(TAG, "duration() - Multi Player is in an invalid state: " + MusydraState.MULTI_PLAYER_IDLE);
            return -1;
        }
        return mMediaPlayer.getDuration();
    }

    public long position() {
        return mMediaPlayer.getCurrentPosition();
    }

    public void seek(long whereto) {
        if ( getState() == MusydraState.MULTI_PLAYER_IDLE ) {
            Log.e(TAG, "seek(" + whereto + ") - Multi Player is in an invalid state: " +
                    MusydraState.MULTI_PLAYER_IDLE);
            return;
        }

        mMediaPlayer.seekTo((int) whereto);
    }

    public void setVolume(float vol) {
        mMediaPlayer.setVolume(vol, vol);
    }

    public void setAudioSessionId(int sessionId) {
        if ( getState() != MusydraState.MULTI_PLAYER_IDLE ) {
            Log.e(TAG, "setAudioSessionId(" + sessionId + ") - Multi Player is not in the IDLE state");
            return;
        }

        mMediaPlayer.setAudioSessionId(sessionId);
    }

    public int getAudioSessionId() {
        return mMediaPlayer.getAudioSessionId();
    }
}
