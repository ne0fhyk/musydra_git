/**
 *
 */

package com.ne0fhyklabs.media.musydra.preferences;

import static com.ne0fhyklabs.media.musydra.Constants.ALBUM_IMAGE;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.ne0fhyklabs.media.musydra.IApolloService;
import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.activities.AudioPlayerHolder;
import com.ne0fhyklabs.media.musydra.service.MusydraService;
import com.ne0fhyklabs.media.musydra.service.ServiceToken;
import com.ne0fhyklabs.media.musydra.service.alljoyn.RemotePlayer;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraStateKey;

/**
 * @author Andrew Neal
 */
public class SettingsHolder extends PreferenceActivity implements ServiceConnection {
    private static final String TAG = MusydraUtils.APP_TAG + ".SettingsHeader";

    public static final String KEY_ALLJOYN_CLIENT_CHANNELS_CATEGORY = "alljoynClientChannelsCategory";

    // Service
    private ServiceToken mToken;

    private final Preference.OnPreferenceChangeListener mPreferenceListener = new Preference.OnPreferenceChangeListener() {

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if ( MusicUtils.mService == null ) {
                Log.d(TAG, "Service binder is not set. Unable to update server state.");
                return false;
            }

            try {
                String prefKey = preference.getKey();

                if ( prefKey.equals(RemotePlayer.PREF_SHARING_ACTIVE) )
                    MusicUtils.mService.setSharingActive((Boolean) newValue);
                else if ( prefKey.equals(RemotePlayer.PREF_GROUP_NAME) ) {
                    // Detect if the channel has a space. Alljoyn doesn't
                    // support channel with space.
                    String userChannel = newValue.toString().trim();
                    String spacePattern = ".*\\s.*";
                    if ( userChannel.matches(spacePattern) ) {
                        // Notify the user
                        MusydraUtils
                        .logErr(getApplicationContext(), "Server name should not contain spaces.");
                        return false;
                    }
                    MusicUtils.mService.setGroupName((String) newValue);
                }

            } catch (RemoteException e) {
                Log.e(TAG, "Remote exception occurred while updating server state.", e);
                return false;
            }

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // This should be called first thing
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        updateServerPreference();
        // ActionBar
        initActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder obj) {
        MusicUtils.mService = IApolloService.Stub.asInterface(obj);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        MusicUtils.mService = null;
    }

    /**
     * Update the ActionBar as needed
     */
    private final BroadcastReceiver mMediaStatusReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ( action.equals(MusydraStateKey.ALLJOYN_STATE.name())
                    || action.equals(RemotePlayer.ACTION_GROUP_NAME_UPDATE) ) {
                updateServerPreference();
            }
            else {
                // Update the ActionBar
                initActionBar();
            }
        }

    };

    @Override
    protected void onStart() {
        // Bind to Service
        mToken = MusicUtils.bindToService(this, this);

        IntentFilter filter = new IntentFilter();
        filter.addAction(MusydraService.META_CHANGED);
        filter.addAction(MusydraService.QUEUE_CHANGED);
        filter.addAction(MusydraService.PLAYSTATE_CHANGED);
        filter.addAction(RemotePlayer.ACTION_GROUP_NAME_UPDATE);
        filter.addAction(MusydraStateKey.ALLJOYN_STATE.name());

        registerReceiver(mMediaStatusReceiver, filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        // Unbind
        if ( MusicUtils.mService != null )
            MusicUtils.unbindFromService(mToken);

        unregisterReceiver(mMediaStatusReceiver);
        super.onStop();
    }

    protected void updateServerPreference() {
        SharedPreferences settings = getPreferenceManager().getSharedPreferences();

        // Configure the preference from settings.xml
        SwitchPreference joynserviceStart = (SwitchPreference) findPreference(RemotePlayer.PREF_SHARING_ACTIVE);
        boolean serviceStartFlag = settings.getBoolean(RemotePlayer.PREF_SHARING_ACTIVE,
                RemotePlayer.DEFAULT_SHARING_ACTIVE);
        joynserviceStart.setChecked(serviceStartFlag);
        joynserviceStart.setOnPreferenceChangeListener(mPreferenceListener);

        EditTextPreference joynserviceName = (EditTextPreference) findPreference(RemotePlayer.PREF_GROUP_NAME);
        String serviceName = settings
                .getString(RemotePlayer.PREF_GROUP_NAME, RemotePlayer.DEFAULT_GROUP_NAME);
        joynserviceName.setText(serviceName);
        joynserviceName.setOnPreferenceChangeListener(mPreferenceListener);

        Preference versionPref = findPreference(getResources().getString(R.string.key_build_version));
        if ( versionPref != null ) {
            try {
                String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                versionPref.setSummary(versionName);
            } catch (NameNotFoundException e) {
                Log.e(TAG, "Unable to resolve version name", e);
            }
        }
    }

    /**
     * Update the ActionBar
     */
    protected void initActionBar() {
        // Custom ActionBar layout
        View view = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
        // Show the ActionBar
        getActionBar().setCustomView(view);
        getActionBar().setTitle(R.string.settings);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setDisplayShowCustomEnabled(true);

        ImageView mAlbumArt = (ImageView) view.findViewById(R.id.action_bar_album_art);
        TextView mTrackName = (TextView) view.findViewById(R.id.action_bar_track_name);
        TextView mAlbumName = (TextView) view.findViewById(R.id.action_bar_album_name);

        String url = MusydraUtils.getImageURL(MusicUtils.getAlbumName(), ALBUM_IMAGE, this);
        AQuery aq = new AQuery(this);
        mAlbumArt.setImageBitmap(aq.getCachedImage(url));

        mTrackName.setText(MusicUtils.getTrackName());
        mAlbumName.setText(MusicUtils.getAlbumName());

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                context.startActivity(new Intent(context, AudioPlayerHolder.class));
                finish();
            }
        });
    }

}
