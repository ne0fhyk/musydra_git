package com.ne0fhyklabs.media.musydra.fragments.section;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.fragments.MusydraFragment;
import com.ne0fhyklabs.media.musydra.fragments.list.RemoteTracksFragment;
import com.ne0fhyklabs.media.musydra.service.alljoyn.RemotePlayer;
import com.ne0fhyklabs.media.musydra.utils.BgHandler;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.utils.PeerGroup;
import com.ne0fhyklabs.media.musydra.utils.PeerGroup.StreamConnection;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraStateKey;

public class StreamBrowserFragment extends MusydraFragment {
    public static final String TAG = StreamBrowserFragment.class.getSimpleName();

    public static final int TITLE_ID = R.string.fragment_streamed_libraries;

    private String mNoPeerGroups;
    private IntentFilter mIntentFilter;
    private CheckBox mDJmodeSwitch;
    private TextView mDJmodeText;
    private RelativeLayout mDJControlsContainer;
    private PullToRefreshListView mPeerGroupsView;

    protected StreamBrowserHandler mHandler;

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if ( MusydraUtils.ACTION_DJ_MODE_CHANGED.equals(action) ) {
                asyncUpdateDJControls();
            }
            else if ( RemotePlayer.ACTION_PEER_GROUPS_UPDATE.equals(action)
                    || MusydraStateKey.ALLJOYN_STATE.name().equals(action) ) {
                asyncUpdateStreamBrowser();
                asyncUpdateDJControls();
            }
            else if ( MusydraUtils.ACTION_SERVICE_STATE.equals(action) ) {
                boolean isServiceConnected = intent.getBooleanExtra(MusydraUtils.KEY_IS_SERVICE_CONNECTED,
                                                                    MusydraUtils.DEFAULT_IS_SERVICE_CONNECTED);
                if ( !isServiceConnected )
                    return;

                asyncUpdateStreamBrowser();
                asyncUpdateDJControls();
            }
        }
    };

    public StreamBrowserFragment() {
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(MusydraUtils.ACTION_DJ_MODE_CHANGED);
        mIntentFilter.addAction(MusydraUtils.ACTION_SERVICE_STATE);
        mIntentFilter.addAction(MusydraStateKey.ALLJOYN_STATE.name());
        mIntentFilter.addAction(RemotePlayer.ACTION_PEER_GROUPS_UPDATE);
    }

    @Override
    public String getTitle() {
        return getResources().getString(TITLE_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stream_browser, container, false);

        final Resources res = getResources();
        mNoPeerGroups = res.getString(R.string.no_peer_groups);

        mPeerGroupsView = (PullToRefreshListView) view.findViewById(R.id.streamChannelList);
        mPeerGroupsView.setOnRefreshListener(new OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                asyncUpdateStreamBrowser();
                asyncUpdateDJControls();
            }
        });

        mDJControlsContainer = (RelativeLayout) view.findViewById(R.id.djControlsContainer);
        mDJControlsContainer.setVisibility(View.GONE);

        mDJmodeText = (TextView) view.findViewById(R.id.djModeText);
        mDJmodeText.setText(res.getString(R.string.dj_mode_label));

        mDJmodeSwitch = (CheckBox) view.findViewById(R.id.djModeSwitch);
        mDJmodeSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MusydraUtils.setDJMode(getActivity().getApplicationContext(), isChecked);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        startBgHandler();

        getActivity().registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    @Override
    public void onResume() {
        super.onResume();

        asyncUpdateStreamBrowser();
        asyncUpdateDJControls();
    }

    @Override
    public void onStop() {
        super.onStop();

        stopBgHandler();

        getActivity().unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public String toString() {
        return TAG;
    }

    protected void asyncUpdateStreamBrowser() {
        mHandler.sendEmptyMessage(StreamBrowserHandler.STREAM_UPDATE);
    }

    protected void asyncUpdateDJControls() {
        mHandler.sendEmptyMessage(StreamBrowserHandler.DJ_UPDATE);
    }

    protected void startBgHandler() {
        if ( mHandler != null )
            return;

        HandlerThread bgThread = new HandlerThread("Stream Browser background handler");
        bgThread.start();
        mHandler = new StreamBrowserHandler(this, bgThread.getLooper());
    }

    protected void stopBgHandler() {
        if ( mHandler == null )
            return;

        mHandler.destroy();
        mHandler = null;
    }

    protected static class StreamBrowserHandler extends BgHandler<StreamBrowserFragment> {

        private static final int DJ_UPDATE = 1;
        private static final int STREAM_UPDATE = 2;
        private static final int JOIN_GROUP = 3;
        private static final int LEAVE_GROUP = 4;
        private static final int ADD_TO_AUTO_CONNECT = 5;
        private static final int REMOVE_FROM_AUTO_CONNECT = 6;

        StreamBrowserHandler(StreamBrowserFragment driver, Looper looper) {
            super(driver, looper);
        }

        @Override
        public void handleMessage(final Message msg) {
            switch (msg.what) {
                case DJ_UPDATE:
                    updateDJControlsContainer();
                    break;

                case STREAM_UPDATE:
                    updateStreamBrowser();
                    break;

                case JOIN_GROUP: {
                    String groupName = (String) msg.obj;
                    joinPeerGroup(groupName);
                    break;
                }

                case LEAVE_GROUP: {
                    String groupName = (String) msg.obj;
                    leavePeerGroup(groupName);
                    break;
                }

                case ADD_TO_AUTO_CONNECT: {
                    String groupName = (String) msg.obj;
                    addToAutoConnectList(groupName);
                    break;
                }

                case REMOVE_FROM_AUTO_CONNECT: {
                    String groupName = (String) msg.obj;
                    removeFromAutoConnectList(groupName);
                    break;
                }

                default:
                    break;
            }
        }

        private void emptyChannelList() {
            setChannelContent(new PeerGroup[] { new PeerGroup(getDriver().mNoPeerGroups) });
        }

        private void updateStreamBrowser() {
            if ( getDriver().mPeerGroupsView == null || MusicUtils.mService == null ) {
                emptyChannelList();
                return;
            }

            try {
                Map<String, PeerGroup> streamChannels = new HashMap<String, PeerGroup>();

                String[] autoConnectList = MusicUtils.mService.getAutoConnectList();
                Set<String> autoConnectSet = new HashSet<String>(Arrays.asList(autoConnectList));

                String[] joinedPeerGroups = MusicUtils.mService.getJoinedPeerGroupList();
                if ( joinedPeerGroups.length > 0 ) {
                    for ( String joinedGroup : joinedPeerGroups ) {
                        PeerGroup pg = new PeerGroup(joinedGroup, StreamConnection.CONNECTED);
                        if ( autoConnectSet.contains(joinedGroup) )
                            pg.mIsAutoConnect = true;

                        streamChannels.put(joinedGroup, pg);
                    }
                }

                String[] foundPeerGroups = MusicUtils.mService.getFoundPeerGroupList();
                if ( foundPeerGroups.length > 0 ) {
                    for ( String foundGroup : foundPeerGroups ) {
                        if ( streamChannels.containsKey(foundGroup) )
                            continue;

                        PeerGroup pg = new PeerGroup(foundGroup);
                        if ( autoConnectSet.contains(foundGroup) )
                            pg.mIsAutoConnect = true;

                        streamChannels.put(foundGroup, pg);
                    }
                }

                if ( streamChannels.isEmpty() )
                    emptyChannelList();
                else
                    setChannelContent(streamChannels.values().toArray(new PeerGroup[0]));

            } catch (RemoteException e) {
                Log.e(TAG, "Remote exception while updating channel list.", e);
            }
        }

        private void setChannelContent(final PeerGroup[] content) {
            final StreamBrowserFragment streamFragment = getDriver();

            if ( streamFragment.mPeerGroupsView == null )
                return;

            final Activity activity = streamFragment.getActivity();
            final Context context = activity.getApplicationContext();

            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    final ListView embeddedListView = streamFragment.mPeerGroupsView.getRefreshableView();

                    if ( content.length == 1 && content[0].mStreamName.equals(streamFragment.mNoPeerGroups) ) {
                        embeddedListView.setAdapter(new ArrayAdapter<PeerGroup>(context,
                                R.layout.listview_item, content));
                    }
                    else {
                        embeddedListView
                        .setAdapter(streamFragment.new StreamArrayAdapter(context, 0, content));
                    }
                    embeddedListView.invalidate();
                    streamFragment.mPeerGroupsView.onRefreshComplete();
                }
            });
        }

        private void updateDJControlsContainer() {
            final StreamBrowserFragment streamFragment = getDriver();

            if ( streamFragment.mDJControlsContainer == null || MusicUtils.mService == null )
                return;

            try {
                final String[] joinedPeerGroups = MusicUtils.mService.getJoinedPeerGroupList();
                streamFragment.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        final String djModeLabel = streamFragment.getResources().getString(
                                                                                           R.string.dj_mode_label);

                        if ( joinedPeerGroups.length > 0 ) {
                            streamFragment.mDJControlsContainer.setVisibility(View.VISIBLE);

                            if ( streamFragment.mDJmodeText != null ) {
                                streamFragment.mDJmodeText.setText(djModeLabel + " "
                                        + joinedPeerGroups[0] + " ( beta ) ");
                            }

                            if ( streamFragment.mDJmodeSwitch != null )
                                streamFragment.mDJmodeSwitch.setChecked(MusydraUtils.getDJMode(streamFragment
                                                                                               .getActivity().getApplicationContext()));
                        }
                        else {
                            streamFragment.mDJControlsContainer.setVisibility(View.GONE);
                            if ( streamFragment.mDJmodeText != null )
                                streamFragment.mDJmodeText.setText(djModeLabel);
                        }
                    }
                });
            } catch (RemoteException e) {
                Log.e(TAG, "Remote exception while updating remote browser bottom controls.", e);
                return;
            }
        }

        private void joinPeerGroup(String channel) {
            if ( MusicUtils.mService == null ) {
                Log.e(TAG, "Service binder is not set. Unable to attempt connection to " + channel);
                return;
            }
            try {
                MusicUtils.mService.joinPeerGroup(channel);
            } catch (RemoteException e) {
                Log.e(TAG, "Remote exception occurred while trying to connect to " + channel, e);
            }
        }

        private void leavePeerGroup(final String peerGroupName) {
            if ( MusicUtils.mService == null ) {
                Log.e(TAG, "Service binder is not set.");
                return;
            }

            try {
                MusicUtils.mService.leavePeerGroup(peerGroupName);
            } catch (RemoteException e) {
                Log.e(TAG, "Remote exception occurred while trying to disconnect.", e);
            }
        }

        private void addToAutoConnectList(final String groupName) {
            if ( MusicUtils.mService == null ) {
                Log.e(TAG, "Service binder is not set.");
                return;
            }

            try {
                MusicUtils.mService.addToAutoConnectList(groupName);
            } catch (RemoteException e) {
                Log.e(TAG, "Remote exception occurred while trying to disconnect.", e);
            }
        }

        private void removeFromAutoConnectList(final String groupName) {
            if ( MusicUtils.mService == null ) {
                Log.e(TAG, "Service binder is not set.");
                return;
            }

            try {
                MusicUtils.mService.removeFromAutoConnectList(groupName);
            } catch (RemoteException e) {
                Log.e(TAG, "Remote exception occurred while trying to disconnect.", e);
            }
        }

    }

    private class StreamArrayAdapter extends ArrayAdapter<PeerGroup> {
        private static final int CONNECT_MENU_ID = 11;
        private static final String CONNECT_MENU_TEXT = "Connect";

        private static final int DISCONNECT_MENU_ID = 12;
        private static final String DISCONNECT_MENU_TEXT = "Disconnect";

        protected static final int AUTO_CONNECT_TOGGLE_ID = 13;
        protected static final String ADD_AUTO_CONNECT_LABEL = "Enable auto connect";
        protected static final String REMOVE_AUTO_CONNECT_LABEL = "Disable auto connect";

        public StreamArrayAdapter(Context context, int textViewResourceId, PeerGroup[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View streamView;
            if ( convertView == null ) {
                final LayoutInflater inflater = getActivity().getLayoutInflater();
                streamView = inflater.inflate(R.layout.listview_items, parent, false);
            }
            else
                streamView = convertView;

            final PeerGroup streamChannel = getItem(position);
            if ( streamChannel == null )
                return streamView;

            TextView streamName = (TextView) streamView.findViewById(R.id.listview_item_line_one);
            streamName.setText(streamChannel.mStreamName);

            TextView streamStatus = (TextView) streamView.findViewById(R.id.listview_item_line_two);
            streamStatus.setText(streamChannel.mConnection.getStatus());

            final ProgressBar progressBar = (ProgressBar) streamView
                    .findViewById(R.id.listview_item_progress_bar);
            if ( streamChannel.mConnection == StreamConnection.CONNECTING ) {
                progressBar.setVisibility(View.VISIBLE);
            }
            else
                progressBar.setVisibility(View.GONE);

            streamView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    switch (streamChannel.mConnection) {
                        case CONNECTED:
                            // Launch the remote browser activity
                            Bundle args = new Bundle(1);
                            args.putString(MusydraUtils.KEY_ALLJOYN_STREAM_NAME, streamChannel.mStreamName);
                            addFragment(RemoteTracksFragment.TAG, args);
                            break;

                        case DISCONNECTED:
                            asyncJoinGroup(streamChannel.mStreamName);
                            streamChannel.mConnection = StreamConnection.CONNECTING;
                            progressBar.setVisibility(View.VISIBLE);
                            notifyDataSetChanged();
                            break;

                        default:
                            break;
                    }
                }
            });

            View contextTip = streamView.findViewById(R.id.track_list_context_frame);
            contextTip.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ( streamChannel.mConnection == StreamConnection.CONNECTING )
                        return;

                    PopupMenu popup = new PopupMenu(getActivity(), v);
                    Menu menu = popup.getMenu();
                    if ( streamChannel.mConnection == StreamConnection.DISCONNECTED ) {
                        menu.add(0, CONNECT_MENU_ID, 0, CONNECT_MENU_TEXT);
                    }
                    else if ( streamChannel.mConnection == StreamConnection.CONNECTED ) {
                        menu.add(0, DISCONNECT_MENU_ID, 0, DISCONNECT_MENU_TEXT);
                    }

                    String autoConnectLabel = streamChannel.mIsAutoConnect ? REMOVE_AUTO_CONNECT_LABEL
                                                                           : ADD_AUTO_CONNECT_LABEL;
                    menu.add(0, AUTO_CONNECT_TOGGLE_ID, 1, autoConnectLabel);

                    popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case CONNECT_MENU_ID:
                                    asyncJoinGroup(streamChannel.mStreamName);
                                    streamChannel.mConnection = StreamConnection.CONNECTING;
                                    progressBar.setVisibility(View.VISIBLE);
                                    notifyDataSetChanged();
                                    return true;

                                case DISCONNECT_MENU_ID:
                                    asyncLeaveGroup(streamChannel.mStreamName);
                                    notifyDataSetChanged();
                                    return true;

                                case AUTO_CONNECT_TOGGLE_ID:
                                    if ( streamChannel.mIsAutoConnect ) {
                                        asyncRemoveFromAutoConnect(streamChannel.mStreamName);
                                    }
                                    else {
                                        asyncAddToAutoConnect(streamChannel.mStreamName);
                                    }

                                    streamChannel.mIsAutoConnect = !streamChannel.mIsAutoConnect;
                                    notifyDataSetChanged();

                                    return true;
                            }
                            return false;
                        }
                    });

                    popup.show();
                }
            });

            return streamView;
        }
    }

    protected void asyncJoinGroup(String groupName) {
        Message msg = mHandler.obtainMessage(StreamBrowserHandler.JOIN_GROUP, groupName);
        mHandler.sendMessage(msg);
    }

    protected void asyncLeaveGroup(String groupName) {
        Message msg = mHandler.obtainMessage(StreamBrowserHandler.LEAVE_GROUP, groupName);
        mHandler.sendMessage(msg);
    }

    protected void asyncAddToAutoConnect(String groupName) {
        Message msg = mHandler.obtainMessage(StreamBrowserHandler.ADD_TO_AUTO_CONNECT, groupName);
        mHandler.sendMessage(msg);
    }

    protected void asyncRemoveFromAutoConnect(String groupName) {
        Message msg = mHandler.obtainMessage(StreamBrowserHandler.REMOVE_FROM_AUTO_CONNECT, groupName);
        mHandler.sendMessage(msg);
    }
}
