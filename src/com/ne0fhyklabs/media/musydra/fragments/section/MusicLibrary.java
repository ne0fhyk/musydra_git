/**
 *
 */

package com.ne0fhyklabs.media.musydra.fragments.section;

import static com.ne0fhyklabs.media.musydra.Constants.MIME_TYPE;
import static com.ne0fhyklabs.media.musydra.Constants.PLAYLIST_RECENTLY_ADDED;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.MediaStore.Audio;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.fragments.MusydraFragment;
import com.ne0fhyklabs.media.musydra.fragments.grid.AlbumsFragment;
import com.ne0fhyklabs.media.musydra.fragments.grid.ArtistsFragment;
import com.ne0fhyklabs.media.musydra.fragments.list.GenresFragment;
import com.ne0fhyklabs.media.musydra.fragments.list.TracksFragment;
import com.ne0fhyklabs.media.musydra.service.MusydraService;

/**
 * @author Andrew Neal
 * @author Fredia Huya-Kouadio
 * @Note This is the "holder" for all of the tabs
 */
public class MusicLibrary extends MusydraFragment {

    public static final String TAG = MusicLibrary.class.getSimpleName();

    public static final int TITLE_ID = R.string.fragment_my_library;

    public static final String KEY_FRAGMENT_IDX = "fragment_index";
    public static final int DEFAULT_FRAGMENT_IDX = 0;

    private ViewPager mLibViewPager;

    @Override
    public String getTitle() {
        return getResources().getString(TITLE_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        // Layout
        View view = inflater.inflate(R.layout.library_browser, container, false);

        LocalMusicPagerAdapter lmpAdapter = new LocalMusicPagerAdapter(getResources(),
                                                                       getChildFragmentManager());

        mLibViewPager = (ViewPager) view.findViewById(R.id.lb_viewPager);
        mLibViewPager.setPageTransformer(true, new DepthPageTransformer());
        mLibViewPager.setPageMargin(getResources().getInteger(
                                                              R.integer.viewpager_margin_width));
        mLibViewPager.setPageMarginDrawable(R.drawable.viewpager_margin);
        mLibViewPager.setOffscreenPageLimit(lmpAdapter.getCount() - 1);
        mLibViewPager.setCurrentItem(DEFAULT_FRAGMENT_IDX);
        mLibViewPager.setAdapter(lmpAdapter);

        // Check if the intent request a specific page
        handleIntentExtras(getArguments());

        return view;
    }

    private void handleIntentExtras(Bundle args) {
        if (args == null)
            return;

        if (args.containsKey(KEY_FRAGMENT_IDX)) {
            int fragmentIdx = args.getInt(KEY_FRAGMENT_IDX, DEFAULT_FRAGMENT_IDX);
            mLibViewPager.setCurrentItem(fragmentIdx, true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter filter = new IntentFilter();
        filter.addAction(MusydraService.META_CHANGED);
    }

    @Override
    public String toString() {
        return TAG;
    }

    private static class LocalMusicPagerAdapter extends FragmentPagerAdapter {

        private final Resources mResources;

        public LocalMusicPagerAdapter(Resources res, FragmentManager fm) {
            super(fm);
            mResources = res;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(final int position) {
            String[] tabTitles = mResources.getStringArray(R.array.tab_titles);
            if ( position >= tabTitles.length )
                return null;

            return tabTitles[position];
        }

        @Override
        public Fragment getItem(final int position) {
            Fragment musicFragment = null;
            switch (position) {
                case 0:
                default:
                    musicFragment = new GenresFragment();
                    break;

                case 1:
                    musicFragment = new ArtistsFragment();
                    break;

                case 2:
                    musicFragment = new AlbumsFragment();
                    break;

                case 3:
                    musicFragment = new TracksFragment();
                    break;

                case 4:
                    Bundle args = new Bundle(2);
                    args.putString(MIME_TYPE, Audio.Playlists.CONTENT_TYPE);
                    args.putLong(BaseColumns._ID, PLAYLIST_RECENTLY_ADDED);

                    musicFragment = new RecentlyAddedFragment();
                    musicFragment.setArguments(args);
                    break;

            }

            return musicFragment;
        }

    }

    private static class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private static float MIN_SCALE = 0.85f;
        private static float MIN_ALPHA = 0.5f;

        @Override
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            int pageHeight = view.getHeight();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                // Modify the default slide transition to shrink the page as well
                float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA + (scaleFactor - MIN_SCALE) / (1 - MIN_SCALE) * (1 - MIN_ALPHA));

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

    private static class DepthPageTransformer implements ViewPager.PageTransformer {
        private static float MIN_SCALE = 0.75f;

        @Override
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
}
