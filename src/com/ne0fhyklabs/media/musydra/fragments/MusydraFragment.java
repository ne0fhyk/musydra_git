package com.ne0fhyklabs.media.musydra.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.activities.MusydraActivity;

public abstract class MusydraFragment extends Fragment {

    protected MusydraActivity mParentActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if ( !(activity instanceof MusydraActivity) )
            throw new RuntimeException(activity.toString() + " must be of type " +
                                       MusydraActivity.class.getSimpleName());

        mParentActivity = (MusydraActivity) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if ( mParentActivity != null ) {
            mParentActivity.setDrawerIndicatorEnabled(isDrawerIndicatorEnabled());
            mParentActivity.setTitle(getTitle());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParentActivity = null;
    }

    public String getTitle() {
        return getResources().getString(R.string.app_name);
    }


    protected void addFragment(String fragmentTag, Bundle args) {
        if ( mParentActivity == null )
            return;

        mParentActivity.addFragment(fragmentTag, args);
    }

    protected boolean isDrawerIndicatorEnabled() {
        return true;
    }

    protected void popContentFragment() {
        if ( mParentActivity == null )
            return;

        mParentActivity.popContentFragment();
    }

    @Override
    public abstract String toString();

}
