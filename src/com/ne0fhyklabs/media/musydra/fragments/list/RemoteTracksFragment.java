package com.ne0fhyklabs.media.musydra.fragments.list;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.fragments.MusydraFragment;
import com.ne0fhyklabs.media.musydra.service.MusydraService;
import com.ne0fhyklabs.media.musydra.utils.MusicUtils;
import com.ne0fhyklabs.media.musydra.utils.MusydraUtils;
import com.ne0fhyklabs.media.musydra.utils.StateManager.MusydraStateKey;
import com.ne0fhyklabs.media.musydra.utils.TrackInfo;
import com.ne0fhyklabs.media.musydra.utils.TrackInfoParcelable;

public class RemoteTracksFragment extends MusydraFragment {
    public static final String TAG = RemoteTracksFragment.class.getSimpleName();

    private static final String MEDIA_DIR = "musydra_downloads";

    private IntentFilter mFilter;
    private PullToRefreshListView mTrackListView;
    private int mListPos = 0;
    private PlayTrackAsyncTask mPlayTask;
    private RemoteTracksRefreshAsyncTask mRefreshTask;
    private RemoteTrackAdapter mRemoteTrackAdapter;

    private String mStreamName;

    private AnimationDrawable mPeakOneAnimation, mPeakTwoAnimation;

    private ImageButton mPrev, mPlay, mNext;

    public ConcurrentHashMap<Long, TrackInfo> mDownloadTracker = new ConcurrentHashMap<Long, TrackInfo>();

    /**
     * Update the list as needed
     */
    private final BroadcastReceiver mMediaStatusReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ( DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action) ) {
                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if ( downloadId == -1 )
                    return;
                TrackInfo downloadInfo = mDownloadTracker.remove(downloadId);
                if ( downloadInfo == null )
                    return;

                toastMessage(downloadInfo.mTitle + " download complete!");
            }
            else if ( MusydraUtils.ACTION_SERVICE_STATE.equals(action) ) {
                refresh();
            }
            else {
                setPauseButtonImage();
                if ( mTrackListView != null && mRemoteTrackAdapter != null ) {
                    mRemoteTrackAdapter.notifyDataSetChanged();
                }
            }
        }

    };

    public RemoteTracksFragment() {
        mFilter = new IntentFilter();
        mFilter.addAction(MusydraService.META_CHANGED);
        mFilter.addAction(MusydraService.QUEUE_CHANGED);
        mFilter.addAction(MusydraService.PLAYSTATE_CHANGED);
        mFilter.addAction(MusydraStateKey.MULTI_PLAYER_STATE.name());
        mFilter.addAction(MusydraUtils.ACTION_SERVICE_STATE);
        mFilter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
    }

    @Override
    public String getTitle() {
        if ( mStreamName != null )
            return mStreamName;

        return super.getTitle();
    }

    @Override
    protected boolean isDrawerIndicatorEnabled() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Bundle arguments = getArguments();
        if ( arguments != null ) {
            mStreamName = arguments.getString(MusydraUtils.KEY_ALLJOYN_STREAM_NAME);
        }

        View view = inflater.inflate(R.layout.remote_tracks, container, false);

        mTrackListView = (PullToRefreshListView) view.findViewById(R.id.remoteTracksList);
        mTrackListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                refresh();
            }
        });



        mPrev = (ImageButton) view.findViewById(R.id.bottom_action_bar_previous);
        mPrev.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if ( MusicUtils.mService == null )
                    return;
                try {
                    if ( MusicUtils.mService.position() < 2000 ) {
                        if ( mTrackListView == null || mRemoteTrackAdapter == null )
                            return;

                        if ( mListPos > 0 ) {
                            mListPos -= 1;
                            startPlay(null, mRemoteTrackAdapter.getItem(mListPos));
                        }
                        // MusicUtils.mService.prev();
                    }
                    else {
                        MusicUtils.mService.seek(0);
                        MusicUtils.mService.play();
                    }
                } catch (RemoteException ex) {
                    ex.printStackTrace();
                }
            }
        });

        mPlay = (ImageButton) view.findViewById(R.id.bottom_action_bar_play);
        mPlay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                doPauseResume();
            }
        });

        mNext = (ImageButton) view.findViewById(R.id.bottom_action_bar_next);
        mNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if ( MusicUtils.mService == null || mTrackListView == null || mRemoteTrackAdapter == null )
                    return;
                // try {
                // // MusicUtils.mService.next();
                // } catch (RemoteException ex) {
                // ex.printStackTrace();
                // }

                if ( mListPos < mRemoteTrackAdapter.getCount() - 1 ) {
                    mListPos += 1;
                    startPlay(null, mRemoteTrackAdapter.getItem(mListPos));
                }
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.remote_browser_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                popContentFragment();
                return true;

            case R.id.remote_disconnect:
                if ( MusicUtils.mService != null ) {
                    try {
                        if ( mStreamName != null )
                            MusicUtils.mService.leavePeerGroup(mStreamName);
                    } catch (RemoteException e) {
                        Log.e(TAG, "Remote exception occurred while disconnecting.", e);
                    }
                }

                popContentFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public String toString() {
        return TAG;
    }

    /**
     * Play and pause music
     */
    private void doPauseResume() {
        try {
            if ( MusicUtils.mService != null ) {
                if ( MusicUtils.mService.isPlaying() ) {
                    MusicUtils.mService.pause();
                }
                else {
                    MusicUtils.mService.play();
                }
            }
            setPauseButtonImage();
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Set the play and pause image
     */
    private void setPauseButtonImage() {
        try {
            if ( MusicUtils.mService != null && MusicUtils.mService.isPlaying() ) {
                mPlay.setImageResource(R.drawable.apollo_holo_light_pause);
            }
            else {
                mPlay.setImageResource(R.drawable.apollo_holo_light_play);
            }
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().registerReceiver(mMediaStatusReceiver, mFilter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(mMediaStatusReceiver);
        stopRefresh();
    }

    public void refresh() {
        stopRefresh();
        startRefresh();
    }

    private void startRefresh() {
        mRefreshTask = new RemoteTracksRefreshAsyncTask();
        mRefreshTask.execute();
    }

    private void stopRefresh() {
        if ( mRefreshTask == null || mRefreshTask.getStatus() == AsyncTask.Status.FINISHED ||
                mRefreshTask.isCancelled() )
            return;

        mRefreshTask.cancel(true);
    }

    private void stopPlay() {
        if ( mPlayTask == null || mPlayTask.getStatus() == AsyncTask.Status.FINISHED ||
                mPlayTask.isCancelled() )
            return;

        mPlayTask.cancel(true);
    }

    private void startPlay(ProgressBar progressBar, TrackInfoParcelable trackInfoP) {
        stopPlay();

        mPlayTask = new PlayTrackAsyncTask(progressBar);
        mPlayTask.execute(trackInfoP);
    }

    public void disconnect() {

    }

    /**
     * Returns a TrackInfo object with updated fields if track is on device.
     * Returns null otherwise
     * 
     * @param mediaInfo
     * @return
     */
    public TrackInfo findTrackOnDevice(TrackInfo mediaInfo) {
        if ( mediaInfo == null )
            return null;

        String selections = MediaStore.Audio.Media.ARTIST + "= ? AND " + MediaStore.Audio.Media.ALBUM +
                "= ? AND " + MediaStore.Audio.Media.TITLE + "= ?";

        String[] selectionArgs = new String[] { mediaInfo.mArtist, mediaInfo.mAlbum, mediaInfo.mTitle };

        Cursor cursor = getActivity().getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                                                 MusicUtils.MUSIC_COLS, selections,
                                                                 selectionArgs, null);

        if ( cursor == null )
            return null;

        TrackInfo foundInfo = null;
        if ( cursor.getCount() > 0 ) {
            cursor.moveToFirst();

            String dataPath = cursor.getString(MusicUtils.DATACOLIDX);
            File dataFile = new File(dataPath);
            if ( dataFile.exists() ) {
                foundInfo = new TrackInfo();
                foundInfo.mTrackId = cursor.getLong(MusicUtils.IDCOLIDX);
                foundInfo.mArtist = cursor.getString(MusicUtils.ARTISTCOLIDX);
                foundInfo.mAlbum = cursor.getString(MusicUtils.ALBUMCOLIDX);
                foundInfo.mTitle = cursor.getString(MusicUtils.TITLECOLIDX);
                foundInfo.mDisplayName = cursor.getString(MusicUtils.DISPLAYNAMECOLIDX);
                foundInfo.mTrackUri = foundInfo.mDownloadUri = dataPath;
                foundInfo.mMimeType = cursor.getString(MusicUtils.MIMETYPECOLIDX);
                foundInfo.mSize = cursor.getLong(MusicUtils.SIZECOLIDX);
                foundInfo.mDateModified = cursor.getLong(MusicUtils.DATE_MODIFIED_COL_IDX);
            }
        }

        cursor.close();
        return foundInfo;
    }

    private void toastMessage(String msg) {
        Log.d(TAG, msg);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private void launchDownload(TrackInfo info) {
        if ( info == null )
            return;

        String externalStorageState = Environment.getExternalStorageState();
        if ( !externalStorageState.equals(Environment.MEDIA_MOUNTED) ) {
            toastMessage("External storage is not mounted.");
            return;
        }

        File mediaStorageDir = new File(
                                        Environment
                                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC),
                                        MEDIA_DIR);

        if ( !mediaStorageDir.exists() ) {
            if ( !mediaStorageDir.mkdirs() ) {
                toastMessage("Failed to create media directory " + mediaStorageDir.getPath());
                return;
            }
        }

        Uri uri = Uri.parse(info.mDownloadUri);
        toastMessage("Downloading " + info.mDisplayName + " to " + mediaStorageDir.getPath());

        DownloadManager dm = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

        DownloadManager.Request request = new DownloadManager.Request(uri)
        .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI)
        .setAllowedOverRoaming(false)
        .setTitle("Musydra download")
        .setDescription("Downloading " + info.mDisplayName)
        .setDestinationInExternalPublicDir(Environment.DIRECTORY_MUSIC,
                                           MEDIA_DIR + File.separator + info.mDisplayName);
        request.allowScanningByMediaScanner();

        mDownloadTracker.put(dm.enqueue(request), info);
    }

    private boolean deleteFile(String filePath) {
        File trackFile = new File(filePath);
        if ( !trackFile.exists() ) {
            toastMessage("Unable to find media file @ " + filePath);
            return false;
        }

        if ( trackFile.delete() ) {
            toastMessage("Deleted media file @ " + filePath);
            return true;
        }
        else
            toastMessage("Unable to delete media file @ " + filePath);
        return false;
    }

    private class RemoteTracksRefreshAsyncTask extends AsyncTask<Void, Void, TrackInfoParcelable[]> {

        private ProgressDialog mProgress;

        @Override
        protected void onPreExecute() {
            if ( mTrackListView == null || !mTrackListView.isRefreshing() ) {
                mProgress = new ProgressDialog(getActivity());
                mProgress.setMessage("Loading tracks");
                mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgress.setIndeterminate(true);
                mProgress.setCancelable(true);
                mProgress.show();
            }
        }

        @Override
        protected TrackInfoParcelable[] doInBackground(Void... params) {
            if ( MusicUtils.mService == null )
                return null;

            try {
                TrackInfoParcelable[] containers = MusicUtils.mService.getAllRemoteTracks();
                return containers;
            } catch (Exception e) {
                Log.e(TAG, "Remote exception occurred while trying to retrieve remote tracks", e);
                return null;
            }
        }

        @Override
        protected void onCancelled(TrackInfoParcelable[] result) {
            if ( mTrackListView != null && mTrackListView.isRefreshing() )
                mTrackListView.onRefreshComplete();

            if ( mProgress != null )
                mProgress.dismiss();
        }

        @Override
        protected void onPostExecute(TrackInfoParcelable[] result) {
            if ( result != null && mTrackListView != null ) {
                mRemoteTrackAdapter = new RemoteTrackAdapter(getActivity(), 0, result);
                mTrackListView.getRefreshableView().setAdapter(mRemoteTrackAdapter);

                if ( mTrackListView.isRefreshing() )
                    mTrackListView.onRefreshComplete();
            }

            if ( mProgress != null )
                mProgress.dismiss();
        }

    }

    private class PlayTrackAsyncTask extends AsyncTask<TrackInfoParcelable, Void, Void> {

        private ProgressBar mProgressBar;
        private ProgressDialog mProgress;

        public PlayTrackAsyncTask(ProgressBar progressBar) {
            super();
            mProgressBar = progressBar;
            if ( progressBar == null ) {
                mProgress = new ProgressDialog(getActivity());
                mProgress.setMessage("Loading track");
                mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgress.setIndeterminate(true);
                mProgress.setCancelable(true);
            }
        }

        @Override
        public void onPreExecute() {
            if ( mProgressBar != null )
                mProgressBar.setVisibility(View.VISIBLE);
            else
                mProgress.show();
        }

        @Override
        protected Void doInBackground(TrackInfoParcelable... params) {
            if ( MusicUtils.mService == null )
                return null;

            int count = params.length;
            for ( int i = 0; i < count; i++ ) {
                if ( isCancelled() )
                    return null;

                TrackInfoParcelable trackInfoP = params[i];

                try {
                    MusicUtils.mService.openRemoteFile(trackInfoP);
                    MusicUtils.mService.play();
                } catch (Exception e) {
                    Log.e(TAG, "Error occurred during remote playback", e);
                }
            }
            return null;
        }

        @Override
        public void onCancelled(Void result) {
            if ( mProgressBar != null )
                mProgressBar.setVisibility(View.GONE);
            else
                mProgress.dismiss();
        }

        @Override
        public void onPostExecute(Void result) {
            if ( mProgressBar != null )
                mProgressBar.setVisibility(View.GONE);
            else
                mProgress.dismiss();
        }

    }

    private class RemoteTrackAdapter extends ArrayAdapter<TrackInfoParcelable> {

        private static final int DELETE_MENU_ID = 21;
        private static final String DELETE_MENU_TEXT = "Delete from device";

        private static final int DOWNLOAD_MENU_ID = 22;
        private static final String DOWNLOAD_MENU_TEXT = "Download";

        public RemoteTrackAdapter(Context context, int textViewResourceId, TrackInfoParcelable[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View trackView;
            if ( convertView == null ) {
                final LayoutInflater inflater = getActivity().getLayoutInflater();
                trackView = inflater.inflate(R.layout.listview_items, parent, false);
            }
            else
                trackView = convertView;

            final TrackInfoParcelable trackInfoP = getItem(position);
            if ( trackInfoP == null )
                return trackView;

            TextView trackTitle = (TextView) trackView.findViewById(R.id.listview_item_line_one);
            trackTitle.setText(trackInfoP.getTrackInfo().mTitle);

            TextView trackArtist = (TextView) trackView.findViewById(R.id.listview_item_line_two);
            trackArtist.setText(trackInfoP.getTrackInfo().mArtist);

            final ProgressBar trackProgress = (ProgressBar) trackView
                    .findViewById(R.id.listview_item_progress_bar);

            trackView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    startPlay(trackProgress, trackInfoP);
                    mListPos = position;
                }
            });

            View contextTip = trackView.findViewById(R.id.track_list_context_frame);
            contextTip.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(getActivity(), v);
                    Menu menu = popup.getMenu();
                    final TrackInfo foundInfo = findTrackOnDevice(trackInfoP.getTrackInfo());
                    if ( foundInfo != null ) {
                        menu.add(0, DELETE_MENU_ID, 0, DELETE_MENU_TEXT);
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case DELETE_MENU_ID:
                                        if ( deleteFile(foundInfo.mTrackUri) ) {
                                            notifyDataSetChanged();
                                            return true;
                                        }
                                }
                                return false;
                            }
                        });
                    }
                    else {
                        menu.add(0, DOWNLOAD_MENU_ID, 0, DOWNLOAD_MENU_TEXT);
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case DOWNLOAD_MENU_ID:
                                        launchDownload(trackInfoP.getTrackInfo());
                                        notifyDataSetChanged();
                                        return true;
                                }
                                return false;
                            }
                        });
                    }
                    popup.show();
                }
            });

            if ( MusicUtils.mService != null ) {
                ImageView peakOne = (ImageView) trackView.findViewById(R.id.peak_one);
                ImageView peakTwo = (ImageView) trackView.findViewById(R.id.peak_two);
                try {
                    if ( trackInfoP.equals(MusicUtils.mService.getCurrentTrack()) ) {
                        peakOne.setImageResource(R.anim.peak_meter_1);
                        peakTwo.setImageResource(R.anim.peak_meter_2);
                        mPeakOneAnimation = (AnimationDrawable) peakOne.getDrawable();
                        mPeakTwoAnimation = (AnimationDrawable) peakTwo.getDrawable();

                        if ( MusicUtils.mService.isPlaying() ) {
                            mPeakOneAnimation.start();
                            mPeakTwoAnimation.start();
                        }
                        else {
                            mPeakOneAnimation.stop();
                            mPeakTwoAnimation.stop();
                        }
                    }
                    else {
                        peakOne.setImageResource(0);
                        peakTwo.setImageResource(0);
                    }
                } catch (RemoteException e) {
                    Log.e(TAG, "Remote exception occurred while retrieving current track.", e);
                }
            }

            return trackView;
        }

    }
}
