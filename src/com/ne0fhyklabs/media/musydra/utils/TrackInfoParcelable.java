package com.ne0fhyklabs.media.musydra.utils;

import android.os.Parcel;
import android.os.Parcelable;

public class TrackInfoParcelable implements Parcelable {

    private TrackInfo mTrackInfo;

    public TrackInfoParcelable(TrackInfo trackInfo) {
        mTrackInfo = trackInfo;
    }

    public TrackInfoParcelable(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<TrackInfoParcelable> CREATOR = new Parcelable.Creator<TrackInfoParcelable>() {
        @Override
        public TrackInfoParcelable createFromParcel(Parcel in) {
            return new TrackInfoParcelable(in);
        }

        @Override
        public TrackInfoParcelable[] newArray(int size) {
            return new TrackInfoParcelable[size];
        }
    };

    public TrackInfo getTrackInfo() {
        return mTrackInfo;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mTrackInfo.mTrackId);
        dest.writeString(mTrackInfo.mArtist);
        dest.writeString(mTrackInfo.mAlbum);
        dest.writeString(mTrackInfo.mTitle);
        dest.writeString(mTrackInfo.mDisplayName);
        dest.writeString(mTrackInfo.mTrackUri);
        dest.writeString(mTrackInfo.mDownloadUri);
        dest.writeString(mTrackInfo.mMimeType);
        dest.writeLong(mTrackInfo.mSize);
        dest.writeLong(mTrackInfo.mDateModified);
    }

    public void readFromParcel(Parcel in) {
        mTrackInfo = new TrackInfo();
        mTrackInfo.mTrackId = in.readLong();
        mTrackInfo.mArtist = in.readString();
        mTrackInfo.mAlbum = in.readString();
        mTrackInfo.mTitle = in.readString();
        mTrackInfo.mDisplayName = in.readString();
        mTrackInfo.mTrackUri = in.readString();
        mTrackInfo.mDownloadUri = in.readString();
        mTrackInfo.mMimeType = in.readString();
        mTrackInfo.mSize = in.readLong();
        mTrackInfo.mDateModified = in.readLong();
    }

    @Override
    public String toString() {
        return mTrackInfo.mTitle + "\n" + mTrackInfo.mArtist;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;

        if (!(other instanceof TrackInfoParcelable))
            return false;

        TrackInfoParcelable that = (TrackInfoParcelable) other;
        return TrackInfo.equals(this.getTrackInfo(), that.getTrackInfo());
    }

    @Override
    public int hashCode() {
        return mTrackInfo.hashCode();
    }

}
