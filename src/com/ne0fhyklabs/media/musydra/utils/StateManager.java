package com.ne0fhyklabs.media.musydra.utils;

import static com.ne0fhyklabs.media.musydra.Constants.ALBUM_IMAGE;

import java.util.concurrent.ConcurrentHashMap;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RemoteViews;

import com.androidquery.AQuery;
import com.ne0fhyklabs.media.musydra.R;
import com.ne0fhyklabs.media.musydra.activities.MusydraActivity;
import com.ne0fhyklabs.media.musydra.fragments.section.StreamBrowserFragment;
import com.ne0fhyklabs.media.musydra.preferences.SettingsHolder;
import com.ne0fhyklabs.media.musydra.service.MediaButtonIntentReceiver;
import com.ne0fhyklabs.media.musydra.service.MusydraService;

public class StateManager {

    private static final String TAG = MusydraUtils.APP_TAG + ".StateManager";

    public static final String KEY_PEER_GROUP_NAME = "peer_group_name_key";
    public static final String KEY_NUM_PEERS = "num_peers_key";
    public static final String KEY_MEDIA_PROPS = "media_props_key";

    private static final int FOREGROUND_NOTIFICATION_ID = 777;
    public static final int ALLJOYN_FOUND_PEER_NOTIFICATION_ID = 12;
    public static final int ALLJOYN_NOTIFICATION_ID = ALLJOYN_FOUND_PEER_NOTIFICATION_ID + 1;

    public enum MusydraStateKey {
        // AllJoyn categories
        ALLJOYN_STATE,

        // MultiPlayer category
        MULTI_PLAYER_STATE
    }

    public enum MusydraState {

        /**
         * AllJoyn states:
         * ====================
         * ALLJOYN_IDLE:
         * *Requirements: None
         * *Description: alljoyn module is not hosting, or hasn't joined any group
         * 
         * ALLJOYN_HOST_GROUP_CREATED:
         * *Requirements: ALLJOYN_STATE == ALLJOYN_IDLE
         * *Description: On user input, the module can create, and host a group, if and only if the
         * module hasn't already joined a remote peer group.
         * 
         * ALLJOYN_PEER_GROUP_JOINED:
         * *Requirements: None
         * *Description: On user input, the module can join a remote peer group. If the module was
         * hosting a peer group prior to joining (ALLJOYN_STATE == ALLJOYN_HOST_GROUP_CREATED),
         * the hosted peer group will be destroyed.
         */
        ALLJOYN_IDLE,
        ALLJOYN_HOST_GROUP_CREATED,
        ALLJOYN_PEER_GROUP_JOINED,

        // MultiPlayer states
        MULTI_PLAYER_IDLE,
        MULTI_PLAYER_PREPARED,
        MULTI_PLAYER_STARTED,
        MULTI_PLAYER_PAUSED,
        MULTI_PLAYER_PLAYBACK_COMPLETED,
        MULTI_PLAYER_END,
    }

    protected ConcurrentHashMap<MusydraStateKey, MusydraState> mStatesHolder;

    private Service mService;
    private Notification mNotification;
    private NotificationManager mNotificationManager;

    public StateManager(Service service) {
        mService = service;
        mNotificationManager = (NotificationManager) mService.getSystemService(Context.NOTIFICATION_SERVICE);

        // Initialize the notification
        initializeNotification();

        // Initialize the states
        initializeStates();
    }

    private void initializeNotification() {
        RemoteViews views = new RemoteViews(mService.getPackageName(), R.layout.status_bar);
        views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
        views.setViewVisibility(R.id.status_bar_album_art, View.GONE);

        views.setTextViewText(R.id.status_bar_track_name, "---");
        views.setTextViewText(R.id.status_bar_artist_name, "---");

        views.setViewVisibility(R.id.status_bar_client_layout, View.GONE);
        views.setTextViewText(R.id.status_bar_client_state, "---");

        views.setViewVisibility(R.id.status_bar_service_layout, View.VISIBLE);
        views.setTextViewText(R.id.status_bar_service_state, "0");
        views.setImageViewResource(R.id.status_bar_play, R.drawable.apollo_holo_dark_play);

        ComponentName rec = new ComponentName(mService.getPackageName(),
                                              MediaButtonIntentReceiver.class.getName());

        Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        mediaButtonIntent.setComponent(rec);

        mediaButtonIntent.putExtra(MusydraService.CMDNOTIF, 1);
        KeyEvent mediaKey = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
        mediaButtonIntent.putExtra(Intent.EXTRA_KEY_EVENT, mediaKey);
        PendingIntent mediaPendingIntent = PendingIntent.getBroadcast(mService.getApplicationContext(), 1,
                                                                      mediaButtonIntent,
                                                                      PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.status_bar_play, mediaPendingIntent);

        mediaButtonIntent.putExtra(MusydraService.CMDNOTIF, 2);
        mediaKey = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_NEXT);
        mediaButtonIntent.putExtra(Intent.EXTRA_KEY_EVENT, mediaKey);
        mediaPendingIntent = PendingIntent.getBroadcast(mService.getApplicationContext(), 2,
                                                        mediaButtonIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.status_bar_next, mediaPendingIntent);

        mediaButtonIntent.putExtra(MusydraService.CMDNOTIF, 3);
        mediaKey = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_STOP);
        mediaButtonIntent.putExtra(Intent.EXTRA_KEY_EVENT, mediaKey);
        mediaPendingIntent = PendingIntent.getBroadcast(mService.getApplicationContext(), 3,
                                                        mediaButtonIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.status_bar_collapse, mediaPendingIntent);

        mediaButtonIntent.putExtra(MusydraService.CMDNOTIF, 4);
        mediaKey = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_CLOSE);
        mediaButtonIntent.putExtra(Intent.EXTRA_KEY_EVENT, mediaKey);
        mediaPendingIntent = PendingIntent.getBroadcast(mService.getApplicationContext(), 4,
                                                        mediaButtonIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.status_bar_exit, mediaPendingIntent);

        Notification.Builder notificationBuilder = new Notification.Builder(mService)
        .setSmallIcon(R.drawable.stat_notify_music)
        .setContent(views)
        .setContentIntent(PendingIntent
                          .getActivity(mService,
                                       0,
                                       new Intent(
                                               "com.ne0fhyklabs.media.musydra.PLAYBACK_VIEWER")
                          .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), 0));

        mNotification = notificationBuilder.getNotification();
    }

    private void initializeStates() {
        mStatesHolder = new ConcurrentHashMap<MusydraStateKey, MusydraState>(MusydraStateKey.values().length);

        mStatesHolder.put(MusydraStateKey.ALLJOYN_STATE, MusydraState.ALLJOYN_IDLE);
        mStatesHolder.put(MusydraStateKey.MULTI_PLAYER_STATE, MusydraState.MULTI_PLAYER_IDLE);
    }

    public MusydraStateKey setState(MusydraState state, Bundle props) {
        MusydraStateKey stateKey = null;
        if ( props == null )
            props = new Bundle();

        switch (state) {
            /* AllJoyn state logic */
            case ALLJOYN_IDLE: {
                stateKey = MusydraStateKey.ALLJOYN_STATE;

                synchronized (mNotification) {
                    mNotification.tickerText = mStatesHolder.get(stateKey) == MusydraState.ALLJOYN_PEER_GROUP_JOINED
                            ? "Disconnected"
                              : "";

                    mNotification.contentView.setTextViewText(R.id.status_bar_client_state, "---");
                    mNotification.contentView.setViewVisibility(R.id.status_bar_client_layout, View.GONE);
                    mNotification.contentView.setViewVisibility(R.id.status_bar_service_layout, View.VISIBLE);
                    mNotification.contentView.setTextViewText(R.id.status_bar_service_state, "0");

                    mNotificationManager.notify(FOREGROUND_NOTIFICATION_ID, mNotification);
                }

                stopForeground(true);
                break;
            }

            case ALLJOYN_PEER_GROUP_JOINED: {
                stateKey = MusydraStateKey.ALLJOYN_STATE;
                final String peerGroupName = props.getString(KEY_PEER_GROUP_NAME);

                synchronized (mNotification) {
                    mNotification.tickerText = "Connected to " + peerGroupName;
                    mNotification.contentView.setTextViewText(R.id.status_bar_client_state, peerGroupName);
                    mNotification.contentView.setViewVisibility(R.id.status_bar_service_layout, View.GONE);
                    mNotification.contentView.setViewVisibility(R.id.status_bar_client_layout, View.VISIBLE);

                    Intent clientPrefIntent = new Intent(mService, MusydraActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(MusydraActivity.KEY_FRAGMENT_TAG, StreamBrowserFragment.TAG);

                    mNotification.contentView
                    .setOnClickPendingIntent(R.id.status_bar_client_state, PendingIntent
                                             .getActivity(mService, 0, clientPrefIntent,
                                                          PendingIntent.FLAG_UPDATE_CURRENT));
                }

                startForeground();
                break;
            }

            case ALLJOYN_HOST_GROUP_CREATED: {
                stateKey = MusydraStateKey.ALLJOYN_STATE;
                int numPeers = props.getInt(KEY_NUM_PEERS);

                synchronized (mNotification) {
                    mNotification.tickerText = mStatesHolder.get(stateKey) == MusydraState.ALLJOYN_PEER_GROUP_JOINED
                            ? "Disconnected"
                              : "";

                    mNotification.contentView.setViewVisibility(R.id.status_bar_client_layout, View.GONE);
                    mNotification.contentView.setViewVisibility(R.id.status_bar_service_layout, View.VISIBLE);
                    mNotification.contentView.setTextViewText(R.id.status_bar_service_state,
                                                              String.valueOf(numPeers));

                    Intent servicePrefIntent = new Intent(mService, SettingsHolder.class);

                    mNotification.contentView
                    .setOnClickPendingIntent(R.id.status_bar_service_state, PendingIntent
                                             .getActivity(mService, 0, servicePrefIntent,
                                                          PendingIntent.FLAG_UPDATE_CURRENT));
                }

                if ( numPeers > 0 )
                    startForeground();
                else
                    stopForeground(true);
                break;
            }

            /* MultiPlayer state logic */
            case MULTI_PLAYER_PREPARED:
                TrackInfoParcelable container = props.<TrackInfoParcelable> getParcelable(KEY_MEDIA_PROPS);
                TrackInfo mediaProps = container.getTrackInfo();

                String albumName = mediaProps.mAlbum;
                String trackName = mediaProps.mTitle;
                String artistName = mediaProps.mArtist;

                AQuery aq = new AQuery(mService);
                Bitmap b = aq.getCachedImage(MusydraUtils.getImageURL(albumName, ALBUM_IMAGE, mService));

                synchronized (mNotification) {
                    if ( b != null ) {
                        mNotification.contentView.setViewVisibility(R.id.status_bar_icon, View.GONE);
                        mNotification.contentView.setViewVisibility(R.id.status_bar_album_art, View.VISIBLE);
                        mNotification.contentView.setImageViewBitmap(R.id.status_bar_album_art, b);
                    }
                    else {
                        mNotification.contentView.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
                        mNotification.contentView.setViewVisibility(R.id.status_bar_album_art, View.GONE);
                    }

                    mNotification.contentView.setTextViewText(R.id.status_bar_track_name, trackName);
                    mNotification.contentView.setTextViewText(R.id.status_bar_artist_name, artistName);

                    mNotification.tickerText = "";
                    mNotification.contentView.setImageViewResource(R.id.status_bar_play,
                                                                   R.drawable.apollo_holo_dark_play);
                }

                stateKey = MusydraStateKey.MULTI_PLAYER_STATE;
                break;

            case MULTI_PLAYER_STARTED:
                synchronized (mNotification) {
                    mNotification.tickerText = "";
                    mNotification.contentView.setImageViewResource(R.id.status_bar_play,
                                                                   R.drawable.apollo_holo_dark_pause);
                }

                startForeground();
                stateKey = MusydraStateKey.MULTI_PLAYER_STATE;
                break;

            case MULTI_PLAYER_PAUSED:
            case MULTI_PLAYER_PLAYBACK_COMPLETED:
                synchronized (mNotification) {
                    mNotification.tickerText = "";
                    mNotification.contentView.setImageViewResource(R.id.status_bar_play,
                                                                   R.drawable.apollo_holo_dark_play);

                    mNotificationManager.notify(FOREGROUND_NOTIFICATION_ID, mNotification);
                }

                stopForeground(true);
                stateKey = MusydraStateKey.MULTI_PLAYER_STATE;
                break;

            case MULTI_PLAYER_IDLE:
            case MULTI_PLAYER_END:
                stopForeground(true);
                stateKey = MusydraStateKey.MULTI_PLAYER_STATE;
                break;

            default:
                Log.e(TAG, "Invalid state: " + state);
                break;
        }

        if ( stateKey != null ) {
            mStatesHolder.put(stateKey, state);

            props.putString(MusydraState.class.getName(), state.name());
            mService.sendBroadcast(new Intent(stateKey.name()).putExtras(props));
        }

        return stateKey;
    }

    public MusydraState getState(MusydraStateKey stateKey) {
        return mStatesHolder.get(stateKey);
    }

    /**
     * Launch notifications based on the id
     */
    public void notify(int notificationId, Object notificationObj) {
        Notification.Builder statusBuilder = new Notification.Builder(mService);

        Intent clientSettingsIntent = new Intent(mService, MusydraActivity.class)
        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
        .putExtra(MusydraActivity.KEY_FRAGMENT_TAG, StreamBrowserFragment.TAG);

        switch (notificationId) {
            case ALLJOYN_FOUND_PEER_NOTIFICATION_ID:
                statusBuilder
                .setTicker("Streams update")
                .setContentTitle((String) notificationObj)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.stat_notify_music)
                .setContentIntent(PendingIntent.getActivity(mService, 0, clientSettingsIntent,
                                                            PendingIntent.FLAG_UPDATE_CURRENT));
                break;

            case ALLJOYN_NOTIFICATION_ID:
                String msg = (String) notificationObj;
                statusBuilder
                .setTicker(msg)
                .setContentTitle(msg)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.stat_notify_music)
                .setContentIntent(PendingIntent.getActivity(mService, 0, clientSettingsIntent,
                                                            PendingIntent.FLAG_UPDATE_CURRENT));
                break;

            default:
                Log.e(TAG, "StateManager.notify - Invalid notification id: " + notificationId);
                return;
        }

        Notification status = statusBuilder.getNotification();
        mNotificationManager.notify(notificationId, status);
    }

    public void notify(String msg) {
        notify(ALLJOYN_NOTIFICATION_ID, msg);
    }

    /**
     * Remove notifications
     */
    public void cancelNotification(int notificationId) {
        mNotificationManager.cancel(notificationId);
    }

    private void startForeground() {
        synchronized (mNotification) {
            mService.startForeground(FOREGROUND_NOTIFICATION_ID, mNotification);
        }
    }

    private void stopForeground(boolean removeNotification) {
        boolean isActivePeer = mStatesHolder.get(MusydraStateKey.ALLJOYN_STATE) == MusydraState.ALLJOYN_PEER_GROUP_JOINED;
        boolean isMultiPlayerPlaying = mStatesHolder.get(MusydraStateKey.MULTI_PLAYER_STATE) == MusydraState.MULTI_PLAYER_STARTED;

        if ( !isActivePeer && !isMultiPlayerPlaying ) {
            mService.stopForeground(removeNotification);
            if ( removeNotification )
                mNotificationManager.cancel(FOREGROUND_NOTIFICATION_ID);
        }
    }
}
