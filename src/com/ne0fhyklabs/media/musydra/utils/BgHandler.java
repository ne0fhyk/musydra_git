package com.ne0fhyklabs.media.musydra.utils;

import java.lang.ref.WeakReference;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public abstract class BgHandler<T> extends Handler {

    private final WeakReference<T> mDriverReference;

    public BgHandler(final T driver, final Looper looper) {
        super(looper);
        mDriverReference = new WeakReference<T>(driver);
    }

    public void destroy() {
        removeCallbacksAndMessages(null);
        getLooper().quit();
        mDriverReference.clear();
    }

    protected T getDriver() {
        return mDriverReference.get();
    }

    @Override
    public abstract void handleMessage(final Message msg);
}
