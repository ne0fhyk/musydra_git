package com.ne0fhyklabs.media.musydra.utils;

import org.alljoyn.bus.annotation.Position;

/**
 * Contains all the information for a track in the media database
 * 
 * @author fhuya
 * 
 */
public class TrackInfo {
    @Position(0)
    public long mTrackId;
    @Position(1)
    public String mArtist;
    @Position(2)
    public String mAlbum;
    @Position(3)
    public String mTitle;
    @Position(4)
    public String mDisplayName;
    @Position(5)
    public String mTrackUri;
    @Position(6)
    public String mDownloadUri;
    @Position(7)
    public String mMimeType;
    @Position(8)
    public long mSize;
    @Position(9)
    public long mDateModified;

    public static TrackInfo copyTrackInfo(TrackInfo original) {
        if (original == null)
            return null;

        TrackInfo copy = new TrackInfo();
        copy.mTrackId = original.mTrackId;
        copy.mArtist = original.mArtist;
        copy.mAlbum = original.mAlbum;
        copy.mTitle = original.mTitle;
        copy.mDisplayName = original.mDisplayName;
        copy.mTrackUri = original.mTrackUri;
        copy.mDownloadUri = original.mDownloadUri;
        copy.mMimeType = original.mMimeType;
        copy.mSize = original.mSize;
        copy.mDateModified = original.mDateModified;

        return copy;
    }

    public static boolean equals(TrackInfo one, TrackInfo two) {
        if (one == two)
            return true;

        return one.mArtist.equals(two.mArtist) && one.mAlbum.equals(two.mAlbum)
                && one.mTitle.equals(two.mTitle);
    }
}
