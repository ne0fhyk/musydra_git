package com.ne0fhyklabs.media.musydra.utils;

public class PeerGroup {

    public enum StreamConnection {
        DISCONNECTED(""),
        CONNECTING("Connecting..."),
        CONNECTED("Connected. Click to access shared media.");

        private String mStatus;

        private StreamConnection(String status) {
            mStatus = status;
        }

        public String getStatus() {
            return mStatus;
        }
    }


    public String mStreamName;
    public StreamConnection mConnection = StreamConnection.DISCONNECTED;
    public boolean mIsAutoConnect = false;

    public PeerGroup(String streamName) {
        mStreamName = streamName;
    }

    public PeerGroup(String streamName, StreamConnection connection) {
        this(streamName);
        mConnection = connection;
    }

    @Override
    public String toString() {
        return mStreamName;
    }

    @Override
    public boolean equals(Object other) {
        if ( this == other )
            return true;

        if ( !(other instanceof PeerGroup) )
            return false;

        PeerGroup that = (PeerGroup) other;
        return mStreamName.equals(that.mStreamName);
    }

    @Override
    public int hashCode() {
        return mStreamName.hashCode();
    }
}
