Winner of AllJoyn™ Peer-2-Peer App Challenge!

Open source (MPL v2) Music player with the ability to stream from, or share your audio to multiple android devices.
Just like the Samsung Galaxy S4, Musydra provides a mode which allow multiple android devices to listen in real time to the shared audio stream.
Utilizes AllJoyn™, an open source application development framework to enable ad hoc, proximity-based device-to-device communication over a wifi network.
NOTES:
Devices must be on the same wifi network to discover each other. Wifi network can be a regular home/business wifi network, or it can be a portable wifi hotspot created by any devices, including the android devices running Musydra.

Features:
- Share your audio to devices under the same wifi network.
- Stream audio from devices under the same wifi network.
- With 'DJ' mode ('Listen With ...'), listen in real time as the audio stream is shared.

Instructions (as noted above, devices must be on the same wifi network):
- To share your music:
1. Open Musydra, access the settings
2. Set up the 'BROADCAST NAME'. That is the name other devices will see.
3. Switch on 'ENABLE MUSYDRA BROADCAST'.
- To stream music from other devices:
1. Open Musydra, and scroll to the leftmost tab 'STREAMS'
2. Switch on 'Stream Browser'.
3. Any devices broadcasting their music should show up under 'Stream Browser'.
4. To connect, click on the name of a streaming device.
5. Once connected, click on the name again to see the tracks available for streaming.
6. Click on a track to stream it.
7. To download a song, click on the downward arrow to the right of the song, and select 'Download'.
- Activate 'DJ' mode:
1. In the 'STREAMS' tab, once connected to another device, a 'Listen With ...' checkbox should appear.
2. Enable the checkbox to activate 'DJ' mode. This allows you to listen to the same song, at the same time, as the device broadcasting.
